

<div align="center"><h1>Lightweight DOTS Utility AI</h1></div>

<div align="center"><img src="Images/demo2.gif"></div>

<div><b>--------------<b></div>
<div><b>[IMPORTANT]<b></div>
<div><b>--------------<b></div>
<ul>
<li>This readme needs to be reworked to reflect the editor/authoring changes introduced by the refactor ( MidnightCow ).</li>
<li>No changes have been done to the DOTS components or systems, only the authoring/editor/setup side, removed Sirenix dependencies, and project structure etc.</li>
<li>UPM AddPackageFromGitURL - https://gitlab.com/MidnightCow/lightweightdotsutilityai.git?path=/com.lightweightdotsutilityai.core
<li>After import, go to Unity main menu Window->LightweightDotsUtilityAI->InitializeData</li>
<li>This will scan for any scriptable assets or create if none found, and then generate enum data in data folder path ('Assets/Scripts/LightweightDotsUtilityAIData/')</li>
<li>It also creates a global scripting define symbol - required so that codebase knows that Enums exist and have been generated.</li>
<li>Package Samples (available to import via UnityPackageManager) REQUIRE Entities Graphics + UniveralRP packages to be installed.</li>
<li>Please use Window->LightweightDOTSUtilityAI->InitializeData after importing Samples.</li>
<li>More details coming soon.</li>
</ul>
<div><b>--------------<b></div>
<br>

Lightweight DOTS Utility AI is an Infinite Axis Utility AI package that is made specifically for DOTS.

<ul>
<li>Compact and designed so that it doesn't intrude in your code - most of your systems can remain untouched and won't directly need to know about the AI.</li>
<li>Uses a pure Data Orientated approach so that scoring is performed with SIMD and systems act asynchronously.</li>
<li>Profiles, Actions, Considerations and Inputs are referred to by IDs so that the only code generation used is for some simple enums.</li>
<li>Support for skipping input calculations optimization (via the NeedsCalculation flag). <b>*New</b></li>
<li>Support for runtime profile swapping/adding/removing (not tested yet)<b>*New</b></li>
</ul>

This readme will not explain the fundamental concepts in Utility AI. To learn more about Utility AI here are some resources:
<ul>
<li>https://www.gdcvault.com/play/1012410/Improving-AI-Decision-Modeling-Through -- <b>STOP AT TIME 33:00!!</b></li>
<li>https://www.gdcvault.com/play/1018040/Architecture-Tricks-Managing-Behaviors-in -- <b>SKIP TO TIME 33:33!!</b></li>
<li>https://archive.org/details/GDC2015Mark</li>
<li>There is a thread in the Unity forums that has a lot of good info where others and I discussed Utility AI implementation in DOTS. It started out as a general DOTS AI thread, but quickly turned to Utility AI since it's such a natural fit for DOTS and ECS. The Utility AI discussion begins around this post: https://forum.unity.com/threads/ai-in-ecs-what-approaches-do-we-have.672310/#post-4505782</li>
</ul>

<b>QUICK TLDR summary:
The general idea is to fill up the input DynamicBuffer for every agent - using as many systems and jobs as you need (it could be just one).
The Utility AI system takes care of scoring the data for you and setting the resulting action as an output (AiActionData).
Then you can use this output to make each agent react appropriately to their decision (again, in your own systems, with as many systems/jobs as you want).
There are a few exceptions for fancy things, but overall that's 90% of the process.
</b>

<hr>
<h3>Installation</h3>

Copy the contents of the <b>LightweightDotsUtilityAi\Assets\LightweightDotsUtilityAiCore</b> folder into your project. It contains the core code, and anything outside of that folder is not part of the core package.

IMPORTANT NOTE!! : At the moment, Odin Inspector is required. I have not had time to remove that dependency.

<hr>
<h3>Setup</h3>

<b>1 - Create 4 scriptable objects.</b>
Create one for Input, Consideration, Decision, and Profile. Do this via right-click in project hierarchy and choose Create/Game/xxxxxxx. There are some examples of setting up these scripts in the example scene.

![screenshot](Images/sofiles.png)

<b>2 - Make an AiTable singleton in the <u>Subscene</u>.</b>
You must place an AiTableAuthoring component on a single game-object in the subscene. It will be a singleton entity, so there must be only one! It will be responsible for baking all the AI rules into a singleton that can be accessed at runtime via SystemAPI.GetSingleton<AiTableData>().]. Assign each field to the appropriate scriptable objects you created in step 1.  It also has some global settings options.

![screenshot](Images/aitable.png)

<b>3 - Add an AiManager singleton to the scene (NOT the subscene).</b>
Place an empty game object in the scene and add the AiManager script to it. This is strictly used by the editor. Set the Enum Output Folder if you want to change the location where the enums are generated. (default is Assets/Scripts/Generated/).

![screenshot](Images/aimanager.png)

<b>4 - Add AiAgentAuthoring to agent prefabs.</b>
Add an AiAgentAuthoring component to your agent prefabs.

![screenshot](Images/agentauth.png)

<b>5 - Add an AiGizmo to the scene.</b>
Place a game-object anywhere in the scene and add the AiGizmo script to it as a component. It will help immensely when debugging.

![screenshot](Images/gizmo.png)

<hr>
<h3>Implementation Details</h3>

The scriptable objects provide a Utility AI authoring interface. The data in the scriptable objects gets converted to a singleton blob asset, which is used at runtime to score decisions for every agent entity. A scoring system will run 4 times (default) per second and each time it is run, all agents will refresh their scores.

The blob asset defines:
<ul>
	<li><b>Inputs</b>. These are the most basic building blocks, and they contain various inputs such as health, distance-to-target, etc. Inputs are often local (such as the agent's health), but they can also come from other locations (such as campaign-mood).  You are responsible for filling these Inputs up, with whatever systems you deem necessary. Every agent has a DynamicBuffer of Inputs on it (floats).</li>
	<li><b>Considerations</b>. These define each "Axis" and represent things to consider when making a decision. It uses a single Input. Every agent has a DynamicBuffer of Consideration scores on it (floats)</li>
	<li><b>Decisions</b>. Each decision is made up of n number of Considerations. The final Decision is designated as the current Action. Every agent has a DynamicBuffer of Decision scores on it (floats)</li>
	<li><b>Profiles</b>. You can define multiple Profiles. For example: simple-enemy, angry-drone, wizard, etc. Each Profile contains a set of Decisions. Once a Decision is chosen, it is placed into the agent's AiActionData so that your various systems can act on the final Decision.</li>
</ul>

Each of the lists above are completely independent of each other so for example, you can reuse the same consideration in multiple decisions.

Every agent entity contains its most recently cached inputs and scores.

![screenshot](Images/aiagententity.png)

As you can see - the entire concept is that you write systems to fill these inputs, and then whenever a score is ready (or at your own leisure) you can check an agent's AiActionData to react to those actions.  It's likely that you already have many of these systems written or purchased, and you just need to adapt them to feed inputs to the AI and react to it.

Within the project there is an Example Scene, which demonstrates a simple good-guy bad-guy scenario. Note that the majority of the code has nothing to do with AI - it's just ordinary systems that a game would use such as movement, damage/health systems, etc.  All the code in that example is NOT part of the core and you won't need it in your project.

Here is what the example agent's buffer's dependencies look like:

![screenshot](Images/single_profile_connections.png){width=50%}

As you can see, many considerations share the same inputs. Likewise, decisions can share considerations, and profiles can share decisions.

<hr>
<h3>Code and Design</h3>

<b>1 - Populate the 4 scriptable objects</b>

The first thing you must do is fill out each of the 4 scriptable objects that you created in step 1 above. You can look at the example project to see how it's done. Most things should be pretty self-explanatory and the tooltips should help guide you. For this part, you'll need to know a little about the philosophy behind Utility AI, so if this part is confusing, you should watch the videos linked above and/or search Unity and Reddit forums for more information.  There is also a little information in the Advanced Concepts section below.

<b>IMPORTANT</b>: Whenever you add/remove/reorder items in the lists, you MUST press the "Generate Enums" button on the AiManager tool so that the enums update.

<b>IMPORTANT</b>: If you modify or delete a name that is used by a higher-level scriptable (such as a consideration name used by a decision), you may end up with an invalid configuration and you'll have reselect a new lower-level item. Currently there is no warning when this happens.

![screenshot](Images/sos.png)

For an example, see: <a href="https://gitlab.com/lclemens/lightweightdotsutilityai/-/blob/master/Assets/Scenes/SampleScene/Data">LightweightDotsUtilityAi\Assets\Scenes\SampleScene\Data</a>

<b>2 - Write one or more "Cheap" Input systems(s)/jobs(s)</b>

Write one or more input filler systems/jobs. I sometimes call them "input-copier" systems because usually they're just copying various variables associated with an agent's state into the agent's input buffer. They are considered "cheap" because running them every time for every agent is considered acceptable.  Most inputs tend to be associated with the entity itself (example: MyHeath, MyStamina, MyDistanceToTarget etc) and your agents likely already have these statistics stored on the agent.  However, some inputs can be global (examples: GlobalEnemyCount, GameTimeRemaining, etc) which will likely be duplicated per agent. It is up to you to decide how many systems you wish to use for filling in the inputs, and it is also up to you how often each input system runs. Personally, I typically just do it in one single input system for convenience.

<b>These cheap input systems should ALWAYS be run in the EarlySimulationSystemGroup.</b>  
The EarlySimulationSystemGroup group comes with the LightWeightDotsUtilityAI package.

See the "Memory management" and "Rate management" sections under the Advanced topic below for hints on optimizing the filling of the input buffer.

For an example, see <a href="https://gitlab.com/lclemens/lightweightdotsutilityai/-/blob/master/Assets/Scenes/SampleScene/Scripts/ExampleCheapInputSystem.cs">LightweightDotsUtilityAi\Assets\Scenes\SampleScene\Scripts\ExampleCheapInputSystem.cs</a>

<b>3 - <u>Optionally Write</u> "Expensive" Systems/Jobs.</b>

Most of your inputs are going to be constantly calculated all the time no matter what (MyHealth, etc). However, sometimes an agent's currently chosen action might enable them to cancel-out (skip) an expensive input calculation entirely. For example, if an agent is on a tropical island his DrinkPiñaColada decision will have a score at or near 1, so there is no point in wasting time calculating distance-to-polar-bears repeatedly because that decision score will be near zero.  In OOP, it's very easy to simply check a decision score against the current high-score and skip running an abstract function if there is no chance that score can become the winner.  But in asynchronous DOP, it's more difficult because the input collection code is sitting in systems that are provided by the game developer.

One way of handling it is to use concepts like <a href="https://www.gdcvault.com/play/1025243/Spatial-Knowledge-Representation-through-Modular">Influence Maps</a>.  Unfortunately, these will likely get calculated all the time (often periodically), but often because of their nature they can often lower spatial-type calculation time overall.

A second way to handle this at a course granularity is via swapping in and out different profiles at runtime - see the Runtime Profile Modification section below under the "Advanced Concepts" heading.

A third more granular way tweak your "expensive" input collection systems to check a "NeedsCalculation" flag. This also has the added benefit of letting you distribute some of the more expensive calculations over different frames instead of running them all at once.  Often if the calculation is heavy enough and you have a lot of agents, it's usually worth trying.

It mainly comes down to checking every agent's NeedsCalculation flag before executing the expensive input code, but you need to do a little more setup first in the AI singleton that's in the subscene.

This is an example of an expensive system and job: <a href="https://gitlab.com/lclemens/lightweightdotsutilityai/-/blob/master/Assets/Scenes/SampleScene/Scripts/ExampleExpensiveTargetSystem.cs">ExampleExpensiveTargetSystem.cs</a>

![screenshot](Images/frame_config.png)

The "Extra Frame Count" setting will insert frames between the beginning and end where you can run expensive systems/jobs.

There are two modes that you can run in:

<b>MODE 1: Auto-Score enabled</b>
This will automatically run a SIMD scoring calculation at the end of every frame, so you don't have to call the scoring function.

```cs
// In your expensive input job, for every agent get write access to the inputs buffer.
public void Execute(ref DynamicBuffer<AiInputData> inputs, ... your other params here... ) 
{
   // Early out if the value is not marked as needing calculation.
   if (!inputs[(int)AiInputType.MyExpensiveInput].NeedsCalculation()) { return; }
   
   // Do your expensive calculations here....
   
   // Copy the expensive calculation result into the input buffer.
   inputs[(int)AiInputType.MyExpensiveInput] = new AiInputData(expensiveCalcResult);
}

// And in your system's OnCreate(), use this to ensure that execution only happens in a designated frame.
// You are responsible for choosing which frames you want which jobs to run in.
// You can use AiFrame1Tag, AiFrame2Tag, etc. Do NOT use AiLastFrameTag.
// Do NOT use a tag number greater than the number of frames you specified in the Extra Frame Count field of the singleton.
state.RequireForUpdate<AiFirstFrameTag>();
```

Philosophical note - It is best to distribute your expensive input jobs such that they are in separate frames becuase it allows an expensive job to possibly cancel out other expensive jobs if it causes a new high-score to appear.  There will be a limited number of frames that you can use depending on your desired target frame rate. For example, if you're targeting 30fps and you set the scoring to run at 0.25ms intervals, then you only get 6 frames total in which to put your jobs.  There is nothing preventing you from stuffing more than one expensive job into a frame as a "phase", however, be aware that when you do that, it won't be possible for one expensive job to cancel out execution the other jobs in that "phase" if it achieves a new high score for an agent. Thus, the ideal situation with auto-score enabled would be one expensive job per frame.

<b>MODE 2: Auto-Score disabled</b>
This will disable the scoring at the end of each frame, so you will need to score your expensive input jobs yourself. There are no frame limitations with this mode, and it will guarantee that every expensive job can cancel other expensive jobs.  However, you must run the scoring calculation at the end of your calculation. This could be a little slower if one of your expensive input jobs is run on the main thread without burst.  I think the vast majority of expensive jobs that run per agent would likely run in parallel threads and use Burst, so most of the time there won't be any slowdown compared to the Auto-Score Enabled method.

```cs
public void Execute(AiAgentAspect ai, ... your other params here... ) 
{
   // Early out if the value is not marked as needing calculation.
   if (!ai.NeedsCalculation(AiInputType.MyExpensiveInput)) { return; }
   
   // Do your expensive calculations here....
   
   // Since auto-score is disabled, you call the scoring function yourself here.
   ai.ScoreInput(AiInputType.MyExpensiveInput, expensiveCalcResult);
}

// In Your system's OnCreate(), you can optionally distribute your expensive inputs over different frames.
// You can use AiFrame1Tag, AiFrame2Tag, etc. Do NOT use AiLastFrameTag.
// Do NOT use a tag number greater than the number of frames you specified in the Extra Frame Count field of the singleton.
state.RequireForUpdate<AiFrameXTag>();
```

If you're curious, here is an example of what the schedules for both methods might look like:

![screenshot](Images/job_timeline_small.png){width=75%}


<b>4 - Write Reaction Systems</b>

As with any AI, you need to execute various functions whenever the agent's AI decides to do something new. For example, you might need to play an animation/sound and damage an enemy when the Attack decision is taken, set the agent's destination whenever the action is one that requires movement, or start construction of a structure if the construct action is chosen.  To do this, you will write one or more systems/jobs for responding to these actions. The query should read AiActionData and any other information it needs to perform the action. By using RequireForUpdate with AiLastFrameTag, you can ensure that your reaction job only runs when a new score has been freshly calculated.  By using AiActionData.IsChanged() you can choose to ignore any action that hasn't changed since the last calculation. AiActionData.OldAction will hold the previous action should you desire to know that. If you already have a game and you're retrofitting Utility AI into it, you likely already have all the systems you need and you can just write some systems to trigger the execution of your existing systems.

For some examples, see <a href="https://gitlab.com/lclemens/lightweightdotsutilityai/-/blob/master/Assets/Scenes/SampleScene/Scripts/ExampleReactionSystem.cs">LightweightDotsUtilityAi\Assets\Scenes\SampleScene\Scripts\ExampleReactionSystem.cs</a>

<hr>
<h3>Advanced Concepts</h3>

<b>1 - "Stacking" Scores</b>
This framework is flexible enough to allow you to "stack" scores because each agent caches its most recent consideration scores.
See https://www.gdcvault.com/play/1012410/Improving-AI-Decision-Modeling-Through at times 23:00-to-25:45 and 30:00-to-32:00 to understand what I mean by "stacking".
If you wanted to implement Mark's example in the video, you would make a new input called "MyNeedToTakeCover" and then periodically fill the corresponding input for it via the equation in Dave's slide - where the inputs to that equation are actually the scores that are cached in the agent's AiConsiderationScoreData DynamicBuffer.  You have the choice of implementing this in a new system/job, or you could simply put it into the standard input-filler agent input system (such as <a href="https://gitlab.com/lclemens/lightweightdotsutilityai/-/blob/master/Assets/Scenes/SampleScene/Scripts/ExampleAgentInputSystem.cs">ExampleAgentInputSystem.cs</a>).

So Mark's equation: "Cover = (0.2 + Reload + (Heal * 1.5)) * (Thread * 1.3)"

would translate to something like:
```cs
Execute(ref DynamicBuffer<AiInputData> inputs, in DynamicBuffer<AiConsiderationScoreData> considerations) {
	inputs[AiInputType.MyNeedToTakeCover] = (0.2f + considerations[AiConsiderationType.MyNeedToReload] + (considerations[AiConsiderationType.MyNeedToHeal] * 1.5f)) * (considerations[AiConsiderationType.MyThreat] * 1.3f);
}
```
Here are some of Dave Mark's slides on the topic:

![screenshot](Images/stacking.png)

<b>2 - Handling Multiple Targets.</b>
Sometimes you have a consideration that needs to consider multiple targets. Examples: AttackTarget (evaluate utility of attacking the closest target), HealAnAlly (evaluate utility of healing the most damaged nearby ally), or ConstructAStructure (evaluate utility of constructing various structures/buildings).  Dave Mark has a quote where he states: *"Any targeted action is scored on a per-target basis. So, the action of 'shoot' would have different scores for 'shoot Bob', 'shoot Ralph', and 'shoot Chuck'. You neither select 'shoot' out of the blue and then decide on a target, nor select a target first and then decide what to do with that target."* For this type of consideration, you need to make your own custom system/job to loop through all potential targets manually (in the same way stacking scores is handled).  In that custom job you can simply write ordinary C# code for choosing a target based on params like distance/health/structure-type/etc.

There is a crude example of a target selection job in <a href="https://gitlab.com/lclemens/lightweightdotsutilityai/-/blob/master/Assets/Scenes/SampleScene/Scripts/ExampleExpensiveTargetSystem.cs">ExampleExpensiveTargetSystem.cs</a>

![screenshot](Images/multitarget.png)

<b>3 - Keeping Tasks Small.</b>
This is a philosophical concept.  One thing I learned in my path to implementing Utility AI is that it's best to keep decisions/actions small and atomic. This is not GOAP! At any given time, ALL actions are being evaluated and although the agent may appear like it's executing a complex goal sequence, in fact, it's simply doing what is convenient and the overall goal-like behavior is emergent. It is similar to how boids-flocking or game-of-life code looks sophisticated when it's actually just a few simple rules that when put together create a complex result. The best way to understand this concept is by reading the firewood collecting example Dave Mark gives in his answer to the question on reddit here: https://www.reddit.com/r/gameai/comments/lj8k3o/infinite_axis_utility_ai_a_few_questions/ .  In my opinion, properly setup Utility AI can stand alone without any other AI techniques, and it can do anything that can be done by traditional GOAP/Behavior Trees/State-Machines/ect.  So, if you keep each task small and atomic, each decision will require more considerations, but overall you won't have to worry about chaining execution order or doing any fancy custom coding.

<b>4 - Runtime Profile Modification.</b>
There may be times when you want to modify an agent's action choices at runtime. An example that Dave gives is a trigger setup such that whenever an agent walks into a bar the agent would acquire an additional profile that would provide the agent with new decisions such as "Buy A Drink", "Chug a Drink", "Punch a Scoundrel", "Belch", etc.  When the agent leaves the bar, that "bar-action" profile would be removed because there is no point in the agent considering chugging beer while running around in the woods fighting enemies.  This is also useful for awarding a character new skills or abilities.  Every agent has a DynamicBuffer containing 1 or more current profiles (AiProfileData) instead of a single profile. At runtime the developer can add/remove profiles based on events like agents going through a tavern door.  I believe at the moment it should work, <u>but I haven't fully tested it</u>.

<b>5 - Memory Management.</b>
For your input buffer, you have many choices on how to fill it up. In theory, you could have all of your systems access it with write-access and fill their slots directly. So for example, anytime an agent gets damaged you could write directly to InputBuffer[AiInputType.MyHealth] and then you wouldn't even need a HealthData component on your entity and you could save memory. However... this has two problems: 1 - This will lock the input buffer so all other jobs must wait for it. 2 - It is intrusive so that you no longer have a generic damage-system and it is coupled to your AI implementation. 

My advice is to keep all your general gaming systems as-is, and then write one or two input-filler systems that do nothing but copy data components such as MyHealth into the input buffer.  It uses more memory, but it results in faster execution and more maintainable code.

Another issue that you may wonder about is why global variables need to be duplicated for every agent (copied to input buffer). The reason involves a tradeoff for speed vs memory. In OOP Utility AI, override functions and/or delegates are used to fetch values when necessary. Since those are difficult to use in DOTS and don't follow data orientated programming philosophy, LightWeightDotsUtilityAI implementation uses a different method.  In order to keep all score calculations running in parallel with SIMD and Burst, duplication of memory is necessary. Fortunately, I have found that 90+% of all variables are per-agent anyways (MyHealth, MyAttackRange, etc), so there is not much data duplication going on.

Regarding caching the Consideration Score and Decision Score buffers on each agent: Technically it is possible to implement without these caches.  However, that causes many issues. It wastes CPU due to redundant calculations and it makes several of the features provided by LightWeightDotsUtilityAI impossible such as stacking and multiple profiles.  Thus, I have decided to cache the agent scores on the agents as a tradeoff to get more flexibility and speed over less memory.

<b>6 - Rate management.</b>

In one of his speeches, Dave Mark states "Data Processing != Decision Processing" and then goes on to explain that you do not need to fill or calculate every value every single frame.

![screenshot](Images/scalability.png)

This not only reduces the amount of calculations you need to run over time, but it lends itself to multithreading and helps with re-using calculation results.

I know... it sounds counterintuitive at first, but what Dave is saying is that if an agent makes a decision a tiny bit late (say 0.25 seconds or less) based on old cached data, the player will never notice.  Say for example, the agent is moving towards a target and the target is destroyed, will the player notice if it keeps moving towards the destroyed agent for an extra 1/4 second (worst case)? 

So the moral of the story is... you don't need to fill inputs at every single frame! For some values, you can only update them on a trigger (like a door opening for example), while others could but put on a timer. I typically run my input-filler system at an interval around 0.2s. The AI scoring system itself defaults to once every 0.25s (1/4 second) and you can change it via the AI singleton in your subscene. I think Dave mentioned somewhere that in one implementation he was running every 1/2 second to 1 second, but I prefer 1/4.  While it is true that a lot of times the input-filler system is copying values that haven't changed, it's also true that it's not running every frame, and it's a simple copy instruction running in parallel with Burst so it should be quite minor compared to the OOP method of invoking virtual functions and other fancy stuff.  If you require deterministic code, you could swap the timer out for something that goes by frame-number instead of time.

In my ECS code in general, I'm often limiting rates to avoid unnecessary calculations. You might think this can cause spikes, but if you do this with a lot of systems throughout your game, you'll end up with a lot of little spikes that when taken in combination, look fairly smooth overall.

![screenshot](Images/areagraph.png){width=50%}

It's better to have a few spikes than needlessly calculate something over and over every frame - essentially that's just making one big, long permanent spike in order to avoid a short spike!

<hr>
<h3>Future Improvements</h3>

<b>1 - Remove Odin Inspector Dependency.</b>
Currently Odin Inspector is required because the default Unity tools do not have enough control over its scriptable object UI. This Odin dependency should be replaced by custom property drawers or perhaps Naughty Attributes.

<b>2 - Handle Multiple Targets Better.</b>
This framework does not handle running against multiple targets automatically.  It can be done, but you must sum the scores manually. It would be nice to have a mechanism to do this, though I'm not sure if there is an easy solution for this.  Even Dave Mark's videos don't truly address the issue. They just sort of assume the targets are chosen, but for some considerations entirely different sets of targets might be chosen. For example, a HomeBaseMorale value might only care about agents near the home base, while a NearbyWizardStrength might only want to examine targets of a specific type nearby. I think this is where Influence Maps would come in handy.

<b>3 - Handle Enum Generation Better.</b>
There should be a better way of generating the enums... regenerating the enum file whenever the designer changes a number is not efficient and can introduce synchronization issues because the domain doesn't reload every time (which would be super slow).  At the moment there is a button that the developer must remember to press after making changes to the scriptable objects. Suggestions?  Also, I think I might remove the dependence on enums within the core in case someone else wants to setup their own GUIs and just reuse the core without depending on enum generation.

<b>4 - Decrease Memory Footprint When Not All Variables Are Used.</b>
Currently all inputs/considerations/decisions are being stored for agents, which allows slots in the buffers to be accessed via an absolute index corresponding with an enum. While this is convenient, it may waste memory in scenarios where a certain agent may not use all input/consideration/decision values.
Saving a fixed mapping in the blob asset would solve this and be very easy to implement. However, it would be very difficult to implement in combination with support of multiple profiles per agent because every profile combination would require a different map. An agent with Profiles A + B + C would need a very different map than an agent with profiles A + C.  The mapping gets even more complicated because there is "sharing" of variables - such as two profiles having considerations that both share the MyHealth input.  I have an idea for solving this, but it has a few drawbacks.

<b>5 - Handle renaming/removing of items in the scriptable object lists.</b> 
Currently if the designer renames an item in a scriptable object list, it will not change the name if that item is referenced in other lists. If the designer deletes an item, no warning is shown alerting the designer that lists referencing the deleted item are broken.
I am unsure what the best way to handle these events is since scriptable object lists have limited support for events.  One possible solution would be to make separate scriptable objects for every single input, consideration, decision, and profile instead of storing them in lists. However, I'm not sure if I like that designer workflow.  I am also considering doing the opposite and putting everything into one scriptable object with 4 tabs (Inputs, Considerations, Decisions and Profiles) - perhaps there is some way in SOs to access other lists within the same SO from inside a struct? Suggestions?

<b>6 - Optimize curve graph property drawer refreshing</b>
At the moment, whenever any parameter is changed in the consideration property sheet, all curve property drawers (animation curves) are redrawn. This only affects the editor. So far it hasn't been a problem, but ideally only the curve that changed should be refreshed and only the mkbc numbers should affect it. This could be fixed by making a separate scriptable object for every consideration. Another idea would be to setup some sort of OnChanged callback with Odin or a property such that it would send a parameter along for the index or name of the curve/graph to refresh.

<b>7 - Use a new curve display and/or add presets.</b>
I may switch to a better-looking curve display that Opeth001 in the forums made. It would be very easy to add a preset combo box, so at the very least I will do that.

<b>8 - Convert project to a package</b>
I would like to make this a package that can be imported via the package manager so that it could be easily upgraded and incorporated into a project. I have never made an importable package before, and I haven't researched how to do it. Hopefully it's not too difficult.

<hr>
<h3>Credits</h3>

- Dave Mark and Mike Lewis (pioneers in Utility AI).
- DreamingImLatios in the Unity forums for all his help with concepts and troubleshooting
- Glamberous in the Turbo Makes Games discord for ideas, help with troubleshooting, and the animation curve.
- Opeth001 in the Unity forums for brainstorming and ideas.
- A bunch of guys in this thread: https://forum.unity.com/threads/ai-in-ecs-what-approaches-do-we-have.672310/
- BadgerMeles in the Unity forums.

<hr>
<h3>Software License</h3>

This software was written to give back to the awesome DOTS community which has helped me so much.

<ol>
<li>You can: Use it in or with a commercial product or game. Zero fees.</li>
<li>You can: Use it in a third party tool (such as a product in the unity asset store) if it is part of a much larger tool. Zero fees.</li>
<li>You cannot: Take it and sell it for profit as a standalone tool with a few tweaks. That's just lame!!</li>
<li>If you give me credit, that would be awesome, but I'm not going to complain if you don't.</li>
<li>If my code messes something up in your project, I'm not taking responsible for it :-)</li>
<li>Good luck with your project!!</li>
</ol>