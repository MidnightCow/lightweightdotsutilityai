# Changelog
All notable changes to this package will be documented in this file. The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## [1.0.0] - 2023-10-25
- Package created - Original project structure refactored from LClemens original repo by MidnightCow.

## [1.0.1] - 2023-10-26
- Updated scriptables in samples, and tool for converting old tables to new versions.

## [1.1.0] - 2023-10-26
- Folders move/rename and couple fixes.

## [1.1.1] - 2023-10-26
- Package readme update and 1.1.1 tag.

## [1.1.2] - 2023-10-26
- Minor package setup bits.

## [1.2.0] - 2023-10-27
- Everything should be working now.
- Visual tweaks to sample scene.

## [2.0.0] - 2023-10-31
- Significant changes to scriptables
- Now one scriptable holds everything
- Scriptable table now does not use the generated enums at all ( int backing instead )
- Custom attr/drawers/editor now handles when lists are reordered/renamed to maintain those values in dependent lists
- For example if you reorder Inputs, any Considerations using those Inputs will still reference the correct Inputs
- Had to clean this up because it was too annoying in my own game
- It does mean you'll have to manually rebuild your Inputs/Considerations/Decisions/Profiles from old versions, sorry
- Removed convert old scriptable tool ( dont have time to update it )

## [2.0.1] - 2023-11-01
- Fixed curves not visually updating in scriptables

## [2.0.2] - 2023-11-01
- Fixed curves showing when considerations collapsed