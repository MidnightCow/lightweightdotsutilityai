// Place this authoring tool on an agent prefab that will go through baking
// to make it controlled by Utility AI systems.

#if LIGHTWEIGHTDOTSUTILITYAI_ENUMS

using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
//SX//SXusing Sirenix.OdinInspector;

namespace Lwduai
{
    public class AiAgentAuthoring : MonoBehaviour
    {
        [Tooltip("The default profiles to use for this agent. They can be swapped in and out at runtime.")]
        //SX//SX[ValueDropdown("@AiOdinUtils.RunProfileNameQuery()", ExcludeExistingValuesInList = true)]
        public List<AiProfileType> Profiles;

        public class AiAgentBaker : Baker<AiAgentAuthoring>
        {
            public override void Bake(AiAgentAuthoring authoring)
            {
                Entity entity = GetEntity(authoring, TransformUsageFlags.None);

                // This holds the current profiles for the agent
                DynamicBuffer<AiProfileData> profiles = AddBuffer<AiProfileData>(entity);
                for (int p = 0; p < authoring.Profiles.Count; p++)
                {
                    profiles.Add(new AiProfileData { ProfileId = authoring.Profiles[p] });
                }

                // This holds a buffer of input data for the agent.
                // It is often a simple copy of another variable (such as a health value).
                // These numbers do not necessarily need to be normalized, but they can be depending on developer preference.
                // One or more systems should be responsible for filling them.
                // Each slot corresponds to an AiInputType enum value.
                DynamicBuffer<AiInputData> inputs = AddBuffer<AiInputData>(entity);
                for (ushort i = 0; i < AiInputTypeSize.Size; i++) { inputs.Add(new AiInputData { Value = 0f, RefCount = 0, IsFreshInput = false }); }

                // This holds a buffer of consideration scores
                // It may be used by some systems to "stack" scores.
                // It also is used by the debugging gizmo.
                // It is also used to cache evaluations so that if multiple actions want to use the same consideration, only one needs to evaluate the curve equation.
                // Each slot corresponds to an AiConsiderationType enum value.
                DynamicBuffer<AiConsiderationScoreData> considerations = AddBuffer<AiConsiderationScoreData>(entity);
                for (ushort c = 0; c < AiConsiderationTypeSize.Size; c++) { considerations.Add(new AiConsiderationScoreData { IsEvaluated = false, Score = 0f }); }

                // This holds a buffer of decision scores for the agent.
                // It may be used by some systems to "stack" scores.
                // It also is used by the debugging gizmo.
                // Each slot corresponds to an AiDecisionType enum value.
                // During a calculation frame, some of the slots may be updated, and then the rest finished at the end of the frame.
                // This creates a temporary inconsistency in the decision buffer.
                // Most reaction systems will use the AiActionData, which won't update until the end of the frame, so it won't affect them.
                // If there are any stacking calculations, they will simply use cached data during that frame, so they should keep working fine.
                DynamicBuffer<AiDecisionScoreData> decisions = AddBuffer<AiDecisionScoreData>(entity);
                for (ushort d = 0; d < AiDecisionTypeSize.Size; d++) { decisions.Add(new AiDecisionScoreData { Score = 0f, IsUsedByProfile = false, TemporaryScore = 0f, IsComplete = false }); }

                // This is the final action that various reaction systems can use to know the current action
                AddComponent(entity, new AiActionData { Action = default, OldAction = default });

                // This keep tracks of the current winning decision.
                AddComponent(entity, new AiWinnerData { Index = -1, Score = float.MinValue });
            }
        }
    }
}

#endif