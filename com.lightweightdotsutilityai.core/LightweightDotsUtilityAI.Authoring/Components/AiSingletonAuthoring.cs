// Typically there is only one of these in the scene.
// You can configure it in the editor.
// It creates a singleton that contains profiles, which are made of decisions,
// which are made of considerations, which are made of inputs.
//
// Press the GenerateEnums button to generate enums that can be used to reference various inputs/considerations/decisions/profiles.
//
// 0.25 * 30 -> 7.5 frames
// 0.25 * 60 --> 15 frames

#if LIGHTWEIGHTDOTSUTILITYAI_ENUMS

using UnityEngine;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Collections;

namespace Lwduai
{
    public class AiSingletonAuthoring : MonoBehaviour
    {
        [Tooltip("These are extra frames that you can use for filling expensive inputs that are action depedent. See docs. (default 0).")]
        [Range(0, 8)]
        public sbyte ExtraFrameCount = 0;

        [Tooltip("Set this to your game's target FPS - it is only used to warn if you if your config is invalid. (default 30)")]
        [Range(3, 240)]
        public ushort TargetFPS = 30;

        [Tooltip("The interval at which scoring happens, measured in seconds. (default 0.25)")]
        [Range(0.01666666666f, 10f)]
        public float ScoringInterval = 0.25f;

        [Tooltip("Auto-runs a scoring at the end of each frame. See docs. (default true)")]
        public bool EnableAutoEndFrameScore = true;

        [Tooltip("A list of all possible inputs")]
        public AiScriptableTable ScriptableTable;

        private void OnGUI()
        {
            if (ExtraFrameCount < 0) {
                UnityEngine.Debug.LogWarning("ExtraFrameCount must be >= 0!!");
            } else if ((ExtraFrameCount + 2) >= (ScoringInterval * TargetFPS)) {
                UnityEngine.Debug.LogWarning($"Extra frame count is {ExtraFrameCount} but there are only {Unity.Mathematics.math.floor(ScoringInterval * TargetFPS) - 2} extra frames available at {TargetFPS} FPS! Increase scoring interval or target FPS, or decrease scoring frame count.");
            }
        }

        public class AiSingletonBaker : Baker<AiSingletonAuthoring>
        {
            public override void Bake(AiSingletonAuthoring authoring)
            {
                if (authoring.ScriptableTable == null)
                {
                    Debug.LogWarning(new System.Exception("AiSingleton Authoring Component requires all component fields to be populated - please check."));
                    return;
                }

                DependsOn(authoring.ScriptableTable);

                Entity entity = GetEntity(authoring, TransformUsageFlags.None);

                AddComponent(entity, new AiTableData { Reference = CreateBlobAsset(authoring.ScriptableTable) });
                AddComponent(entity, new AiFrameStateData { FrameNumber = -1 });
                AddComponent(entity, new AiScoreConfigData { LastFrame = (sbyte)(authoring.ExtraFrameCount + 1), ScoringInterval = authoring.ScoringInterval, EnableAutoEndFrameScore = authoring.EnableAutoEndFrameScore });
            }

            // takes an AiTableEntry (which wraps up scriptable-objects into a single object), and returns a blob asset reference.
            public BlobAssetReference<AiTableDef> CreateBlobAsset(AiScriptableTable scriptableTable)
            {
                using BlobBuilder builder = new BlobBuilder(Allocator.Temp); // using ensures that it's auto-disposed
                ref AiTableDef blobAsset = ref builder.ConstructRoot<AiTableDef>();

                // input array
                BlobBuilderArray<AiInputDef> inputs = builder.Allocate(ref blobAsset.Inputs, scriptableTable.Inputs.Count);
                for (ushort i = 0; i < scriptableTable.Inputs.Count; i++)
                {
                    string inputString = scriptableTable.Inputs[i].Name;
                    // set the value of this input.
                    // ContributingConsiderationIds will be set below.
                    inputs[i] = new AiInputDef { InputId = (AiInputType)i };

                    // make a list of all consideration ids that use this input (either as a direct input or for min/max range value)
                    List<AiConsiderationType> considerationIdList = new List<AiConsiderationType>();
                    for (ushort c = 0; c < scriptableTable.Considerations.Count; c++)
                    { // check ALL possible considerations
                        var consideration = scriptableTable.Considerations[c];
                        if (consideration.Input == i)
                        {
                            considerationIdList.Add((AiConsiderationType)c);
                        }
                        if (consideration.EnableRanges && consideration.Ranges.MaxIsInput && (consideration.Ranges.MaxInput == i))
                        {
                            considerationIdList.Add((AiConsiderationType)c);
                        }
                        if (consideration.EnableRanges && consideration.Ranges.MinIsInput && (consideration.Ranges.MinInput == i))
                        {
                            considerationIdList.Add((AiConsiderationType)c);
                        }
                    }
                    // allocate the considerationIds blob-array and fill it to match the previously created consideration id list.
                    BlobBuilderArray<AiConsiderationType> considerationTypeRefs = builder.Allocate(ref inputs[i].ContributingConsiderationIds, considerationIdList.Count);
                    for (ushort c = 0; c < considerationIdList.Count; c++)
                    {
                        considerationTypeRefs[c] = considerationIdList[c];
                    }
                }

                // consideration array
                BlobBuilderArray<AiConsiderationDef> considerations = builder.Allocate(ref blobAsset.Considerations, scriptableTable.Considerations.Count);
                for (ushort c = 0; c < scriptableTable.Considerations.Count; c++)
                {
                    AiConsiderationEntry considerationDef = scriptableTable.Considerations[c];
                    //RangeMaskType rangeMask = RangeMaskType.None;
                    byte rangeMask = 0;
                    float min = 0f, max = 1f; // default zero to one even if ranges are disabled
                    if (considerationDef.EnableRanges)
                    {
                        if (considerationDef.Ranges.MinIsInput)
                        {
                            rangeMask = BitmaskUtils.SetFlag(rangeMask, (byte)RangeMaskType.Min);
                            // interpret the min float as an integer corresponding to the input-id enum
                            min = (float)considerationDef.Ranges.MinInput;
                        }
                        else
                        {
                            min = considerationDef.Ranges.Min;
                        }
                        if (considerationDef.Ranges.MaxIsInput)
                        {
                            rangeMask = BitmaskUtils.SetFlag(rangeMask, (byte)RangeMaskType.Max);
                            // interpret the max float as an integer corresponding to the input-id enum
                            max = (float)considerationDef.Ranges.MaxInput;
                        }
                        else
                        {
                            max = considerationDef.Ranges.Max;
                        }
                    }
                    // set the value of this consideration.
                    // ContributingDecisionIds will be set below.
                    considerations[c] = new AiConsiderationDef
                    {
                        ConsiderationId = (AiConsiderationType)c,
                        Curve = considerationDef.Curve,
                        InputId = (AiInputType)considerationDef.Input,
                        RangeMask = rangeMask,
                        Min = min,
                        Max = max
                    };

                    // make a list of all decision ids that use this input
                    List<AiDecisionType> decisionIdList = new List<AiDecisionType>();
                    for (ushort d = 0; d < scriptableTable.Decisions.Count; d++)
                    {
                        AiDecisionEntry decision = scriptableTable.Decisions[d];
                        foreach (var consideration in decision.Considerations)
                        {
                            if (consideration == c) decisionIdList.Add((AiDecisionType)c);                            
                        }
                    }
                    // allocate the decisionIds blob-array and fill it to match the previously created decision id list.
                    BlobBuilderArray<AiDecisionType> refs = builder.Allocate(ref considerations[c].ContributingDecisionIds, decisionIdList.Count);
                    for (ushort d = 0; d < decisionIdList.Count; d++)
                    {
                        // TODO : Check if this should be adding duplicates..
                        refs[d] = decisionIdList[d];
                    }
                }

                // decision array
                BlobBuilderArray<AiDecisionDef> destDecisions = builder.Allocate(ref blobAsset.Decisions, scriptableTable.Decisions.Count);
                for (ushort d = 0; d < scriptableTable.Decisions.Count; d++)
                {
                    ref AiDecisionDef destDecision = ref destDecisions[d];
                    AiDecisionEntry srcDecision = scriptableTable.Decisions[d];
                    BlobBuilderArray<AiConsiderationType> considerationIds = builder.Allocate(ref destDecision.ConsiderationIds, srcDecision.Considerations.Count);
                    for (ushort c = 0; c < srcDecision.Considerations.Count; c++)
                    {
                        considerationIds[c] = (AiConsiderationType)srcDecision.Considerations[c];
                    }
                    destDecisions[d].DecisionId = (AiDecisionType)d;
                    destDecisions[d].IsEnabled = srcDecision.IsEnabled;
                    destDecisions[d].Weight = srcDecision.Weight;
                }

                // profile array
                BlobBuilderArray<AiProfileDef> profiles = builder.Allocate(ref blobAsset.Profiles, scriptableTable.Profiles.Count);
                for (ushort p = 0; p < scriptableTable.Profiles.Count; p++)
                {
                    ref AiProfileDef destProfile = ref profiles[p];
                    AiProfileEntry srcProfile = scriptableTable.Profiles[p];
                    profiles[p].ProfileId = (AiProfileType)p;
                    BlobBuilderArray<AiDecisionType> destDecisionIds = builder.Allocate(ref destProfile.DecisionIds, srcProfile.Decisions.Count);
                    for (ushort d = 0; d < srcProfile.Decisions.Count; d++)
                    {
                        destDecisionIds[d] = (AiDecisionType)srcProfile.Decisions[d];
                    }
                }

                // create blob asset reference
                BlobAssetReference<AiTableDef> blobRef = builder.CreateBlobAssetReference<AiTableDef>(Allocator.Persistent);

                //builder.Dispose();
                return blobRef;
            }
        }
    }
}
#endif