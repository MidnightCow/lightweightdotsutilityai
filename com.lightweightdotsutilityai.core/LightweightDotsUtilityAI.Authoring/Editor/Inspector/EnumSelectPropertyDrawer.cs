﻿using UnityEditor;
using UnityEngine;

namespace Lwduai
{
    // This Attribute and Drawer just allows us to show string
    // names ( enum names ) on the scriptables and a dropdown
    // when the field itself is just an int representing the
    // enum index.

    public class EnumSelectAttribute : PropertyAttribute
    {
        public string enumtype;
        public EnumSelectAttribute(string type) => enumtype = type;
    }
    [CustomPropertyDrawer(typeof(EnumSelectAttribute))]
    public class EnumSelectAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.LabelField(position, label);
            position.x += EditorGUIUtility.labelWidth + 2;
            position.width = position.width - EditorGUIUtility.labelWidth - 2;

            var itemIndex = property.intValue;
            var scriptableObject = property.serializedObject;
            var enumType = ((EnumSelectAttribute)attribute).enumtype;
            var sourceArray = scriptableObject.FindProperty(enumType);
            var labelString = GetItem(sourceArray, itemIndex, enumType);

            DrawDropdown(position, labelString, property, sourceArray, enumType, this);
        }
        static void DrawDropdown(Rect position, string labelString, SerializedProperty property, SerializedProperty sourceArray, string enumType, PropertyDrawer drawer)
        {
            if (!EditorGUI.DropdownButton(position, new GUIContent(labelString), FocusType.Passive)) return;

            void handleItemClicked(object parameter)
            {
                property.intValue = (int)parameter;
                property.serializedObject.ApplyModifiedProperties();
            }

            GenericMenu menu = new GenericMenu();
            for (int i = 0; i < sourceArray.arraySize; i++)
            {
                var str = GetItem(sourceArray, i, enumType);
                menu.AddItem(new GUIContent(str), false, handleItemClicked, i);
            }
            menu.DropDown(position);
        }
        static string GetItem(SerializedProperty sourceArray, int itemIndex, string enumType)
        {
            return sourceArray.GetArrayElementAtIndex(itemIndex).FindPropertyRelative("Name").stringValue;
        }
    }
}
