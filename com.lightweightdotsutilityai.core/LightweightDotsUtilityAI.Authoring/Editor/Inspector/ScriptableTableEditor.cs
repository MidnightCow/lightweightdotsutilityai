﻿using UnityEditor;
using UnityEngine;
using UnityEditorInternal;
using System.Collections.Generic;
using UnityEngine.UIElements;
using System.Security.Cryptography;

namespace Lwduai
{
    // The sole reason for this Custom Editor and Custom lists is to get callbacks for the lists
    // on the scriptable table where we can update all dependencies when a list is reordered
    // in the editor.

    // For example if i reorder the Inputs list, the callback will check all Considerations and
    // make sure that the fields using Inputs are still pointing to the correct Inputs after
    // the reordering.

    // All the other stuff is just to ensure these Custom lists are rendered properly.

    [CustomEditor(typeof(AiScriptableTable), true)]
    public class ScriptableTableEditor : Editor
    {
        Dictionary<string, CustomReorderableList> CustomLists;        
        
        private void OnEnable()
        {
            var inputs = serializedObject.FindProperty("Inputs");
            var considerations = serializedObject.FindProperty("Considerations");
            var decisions = serializedObject.FindProperty("Decisions");
            var profiles = serializedObject.FindProperty("Profiles");

            CustomLists = new Dictionary<string, CustomReorderableList>
            {
                { "Inputs",         new CustomReorderableList(ref inputs, ref considerations,       "Inputs") },
                { "Considerations", new CustomReorderableList(ref considerations, ref decisions,    "Considerations") },
                { "Decisions",      new CustomReorderableList(ref decisions, ref profiles,      "Decisions") },
            };
        }
        public override void OnInspectorGUI()
        {
            // Render Custom lists
            foreach (var customList in CustomLists.Values) customList.RenderList();

            // PROFILES is the only one that doesn't need a custom list
            EditorGUILayout.PropertyField(serializedObject.FindProperty("Profiles"));

            serializedObject.ApplyModifiedProperties();

            var buttonStyle = new GUIStyle(EditorStyles.miniButton)
            {
                alignment = TextAnchor.MiddleCenter,
                fontStyle = FontStyle.Normal,
                fixedHeight = 50,
                fontSize = 17,
            };

            if (GUILayout.Button("InitializeData", buttonStyle)) AiDataUtils.ParseAllUserData();
        }

        public class CustomReorderableList
        {
            public bool ShowList;
            public bool[] ShowItem;
            private ReorderableList List;
            private string ListPropertyName;
            private SerializedProperty SourcePropList;            
            private SerializedProperty ArrayThatUsesThisList;            
            public CustomReorderableList(ref SerializedProperty sourcePropList, ref SerializedProperty arrayThatUsesThisList, string propName)
            {
                SourcePropList = sourcePropList;
                ArrayThatUsesThisList = arrayThatUsesThisList;
                ListPropertyName = propName;

                ShowItem = new bool[sourcePropList.arraySize];
                List = new ReorderableList(SourcePropList.serializedObject, SourcePropList, true, false, true, true);
                List.drawElementCallback = Callback_OnDrawItem;
                List.elementHeightCallback = Callback_OnGetItemHeight;
                List.onReorderCallbackWithDetails = Callback_OnListReordered;
            }
            public void RenderList()
            {
                var origFontStyle = EditorStyles.label.fontStyle;
                EditorStyles.foldout.fontStyle = FontStyle.Bold;
                ShowList = EditorGUILayout.Foldout(ShowList, ListPropertyName, EditorStyles.boldFont);
                EditorStyles.label.fontStyle = origFontStyle;
                if (ShowList) List.DoLayoutList();

            }
            void Callback_OnDrawItem(Rect rect, int index, bool isActive, bool isFocused)
            {
                EditorGUI.indentLevel++;
                SerializedProperty element = SourcePropList.GetArrayElementAtIndex(index);
                EditorGUIUtility.labelWidth = rect.width / 2.5f;
                rect.height = EditorGUIUtility.singleLineHeight;
                EditorGUI.PropertyField(
                    rect,
                    element,
                    new GUIContent(element.FindPropertyRelative("Name").stringValue),
                    true
                );
                if (ListPropertyName == "Considerations")
                {
                    var singleBoxHeight = EditorGUI.GetPropertyHeight(element, false);
                    var fullBoxHeight = EditorGUI.GetPropertyHeight(element, true);
                    if (fullBoxHeight != EditorGUIUtility.singleLineHeight)
                    {
                        EditorGUI.indentLevel++;
                        rect.y += fullBoxHeight;
                        rect.height = singleBoxHeight * 3f;

                        var animCurve = GetUpdatedCurve(ref element);
                        EditorGUI.CurveField(rect, animCurve);//, Color.green, boundedCurve.bounds);
                        EditorGUI.indentLevel--;
                    }
                }
                EditorGUI.indentLevel--;
            }
            float Callback_OnGetItemHeight(int index)
            {
                var element = SourcePropList.GetArrayElementAtIndex(index);
                var height = EditorGUI.GetPropertyHeight(element, true);
                if (ListPropertyName == "Considerations")
                {
                    var singleBoxHeight = EditorGUI.GetPropertyHeight(element, false);
                    var fullBoxHeight = EditorGUI.GetPropertyHeight(element, true);
                    if (fullBoxHeight != EditorGUIUtility.singleLineHeight) height += singleBoxHeight * 3f;
                }
                return height;
            }
            void Callback_OnListReordered(ReorderableList list, int oldIndex, int newIndex)
            {
                for (int i = 0; i < ArrayThatUsesThisList.arraySize; i++)
                {
                    var inputItem = ArrayThatUsesThisList.GetArrayElementAtIndex(i); // consideration or decision or profile item
                    var fieldProp = inputItem;

                    if (ListPropertyName == "Inputs")
                    {
                        fieldProp = inputItem.FindPropertyRelative("Input"); // get input field from consideration item
                        ReorderField(ref fieldProp, oldIndex, newIndex);
                        var rangeProp = inputItem.FindPropertyRelative("Ranges");
                        fieldProp = rangeProp.FindPropertyRelative("MinInput");
                        ReorderField(ref fieldProp, oldIndex, newIndex);
                        fieldProp = rangeProp.FindPropertyRelative("MaxInput");
                        ReorderField(ref fieldProp, oldIndex, newIndex);
                    }
                    else if (ListPropertyName == "Considerations")
                    {
                        var considerationsArray = inputItem.FindPropertyRelative("Considerations"); // get considerations list from decision item
                        for (int c = 0; c < considerationsArray.arraySize; c++)
                        {
                            fieldProp = considerationsArray.GetArrayElementAtIndex(c);
                            ReorderField(ref fieldProp, oldIndex, newIndex);
                        }
                    }
                    else if (ListPropertyName == "Decisions")
                    {
                        var decisionsArray = inputItem.FindPropertyRelative("Decisions"); // get decisions list from profile item
                        for (int c = 0; c < decisionsArray.arraySize; c++)
                        {
                            fieldProp = decisionsArray.GetArrayElementAtIndex(c);
                            ReorderField(ref fieldProp, oldIndex, newIndex);
                        }
                    }
                }
                ArrayThatUsesThisList.serializedObject.ApplyModifiedProperties();
            }
            void ReorderField(ref SerializedProperty fieldProp, int oldIndex, int newIndex)
            {
                var thisIndex = fieldProp.intValue;
                var thisIndexNew = fieldProp.intValue;

                if (thisIndex < oldIndex)
                {
                    if (thisIndex < newIndex)
                    {
                        // stays same
                    }
                    else if (thisIndex >= newIndex)
                    {
                        thisIndexNew++;
                    }
                }
                else if (thisIndex == oldIndex)
                {
                    thisIndexNew = newIndex;
                }
                else if (thisIndex > oldIndex)
                {
                    if (thisIndex <= newIndex)
                    {
                        thisIndexNew--;
                    }
                    else if (thisIndex > newIndex)
                    {
                        // stays same
                    }
                }
                fieldProp.intValue = thisIndexNew;
            }
            AnimationCurve GetUpdatedCurve(ref SerializedProperty element)
            {
                var animCurve = element.FindPropertyRelative("CurveDisplay").animationCurveValue;
                var curveData = element.FindPropertyRelative("Curve");
                var curveDataClass = new CurveData
                {
                    CurveType = (CurveType)curveData.FindPropertyRelative("CurveType").intValue,
                    K = curveData.FindPropertyRelative("K").floatValue,
                    M = curveData.FindPropertyRelative("M").floatValue,
                    B = curveData.FindPropertyRelative("B").floatValue,
                    C = curveData.FindPropertyRelative("C").floatValue,
                };

                animCurve.keys = new Keyframe[0];
                for (int s = 0; s <= 20; s++)
                {
                    var t = s * 0.05f;
                    animCurve.AddKey(t, CurveUtils.Evaluate(t, curveDataClass));
                    AnimationUtility.SetKeyLeftTangentMode(animCurve, s, UnityEditor.AnimationUtility.TangentMode.Linear);
                    AnimationUtility.SetKeyRightTangentMode(animCurve, s, UnityEditor.AnimationUtility.TangentMode.Linear);
                }

                return animCurve;
            }
        }
    }
}