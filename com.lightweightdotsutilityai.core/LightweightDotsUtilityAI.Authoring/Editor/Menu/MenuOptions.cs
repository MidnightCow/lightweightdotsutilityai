﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Lwduai
{
    public class MenuOptions
    {
        [MenuItem("Window/LightweightDOTSUtilityAI/InitializeData", priority = 100000)]
        private static void InitializeData(MenuCommand menuCommand) => AiDataUtils.ParseAllUserData();
    }
}
