// This scriptable object defines a list of considerations.
// **Note that it requires BoundedCurve, which can be found in CurvePropertyDrawer.cs

// **Note - it is very inefficient right now - anytime you modify a setting, it will redraw all the curves.
//   Unfortunately, there is no easy way around this, so for now it will have to suffice.
//   This inefficiency only affects the editor.

using UnityEngine;

namespace Lwduai
{
    [System.Serializable]
    public class AiRangeEntry
    {
        [Tooltip("Checking this will allow the min value to be obtained from an input. (default false)")]
        public bool MinIsInput = false;

        [Tooltip("The input value to use if MinIsInput is checked. (default first input)")]
        [ConditionalHideAttribute("MinIsInput", true)]
        [EnumSelectAttribute("Inputs")]
        public int MinInput;

        [Tooltip("The static value used if the MinIsInput is unchecked. (default 0)")]
        [ConditionalHideAttribute("MinIsInput", false, true)]
        public float Min = 0f;

        [Tooltip("Checking this will allow the max value to be obtained from an input. (default false)")]
        public bool MaxIsInput = false;

        [Tooltip("The input value to use if MaxIsInput is checked. (default first input)")]
        [ConditionalHideAttribute("MaxIsInput", true)]
        [EnumSelectAttribute("Inputs")]
        public int MaxInput;

        [Tooltip("The static value used if the MaxIsInput is unchecked. (default 1)")]
        [ConditionalHideAttribute("MaxIsInput", false, true)]
        public float Max = 1f;
    }

    [System.Serializable]
    public class AiConsiderationEntry : AiTableRecordEntry
    {
        [Tooltip("The input to use for the value in this consideration. (default first input)")]
        [EnumSelectAttribute("Inputs")]
        public int Input;

        [Tooltip("Specify the curve type. (default Linear)")]
        public CurveData Curve = new CurveData { M = 0, K = 0, B = 0, C = 0, CurveType = CurveType.Linear };

        [Tooltip("Default range is 0 to 1. Enable this to set custom ranges. (default disabled)")]
        public bool EnableRanges = false;

        [ConditionalHideAttribute("EnableRanges", true)]
        public AiRangeEntry Ranges = new AiRangeEntry();

        [HideInInspector]
        [SerializeField]
        private AnimationCurve CurveDisplay = new AnimationCurve();

        //// Nonlinear version
        //public void RefreshCurveDisplay()
        //{
        //    CurveDisplay.keys = new Keyframe[0];
        //    for (int i = 0; i <= 20; i++) {
        //        float sample = i * 0.05f;
        //        CurveDisplay.AddKey(sample, CurveUtils.Evaluate(sample, Curve));
        //    }
        //}
        // Linear version
        //public void RefreshCurveDisplay()
        //{
        //    CurveDisplay.keys = new Keyframe[0];
        //    for (int s = 0; s <= 20; s++) {
        //        var t = s * 0.05f;
        //        CurveDisplay.AddKey(t, CurveUtils.Evaluate(t, Curve));                
        //        UnityEditor.AnimationUtility.SetKeyLeftTangentMode(CurveDisplay, s, UnityEditor.AnimationUtility.TangentMode.Linear);
        //        UnityEditor.AnimationUtility.SetKeyRightTangentMode(CurveDisplay, s, UnityEditor.AnimationUtility.TangentMode.Linear);
        //    }
        //}
    }
}