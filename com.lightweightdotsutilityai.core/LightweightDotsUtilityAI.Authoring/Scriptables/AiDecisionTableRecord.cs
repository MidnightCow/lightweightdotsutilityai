using UnityEngine;
using System.Collections.Generic;

namespace Lwduai
{
    [System.Serializable]
    public class AiDecisionEntry : AiTableRecordEntry
    {
        [Tooltip("Weight is an extra fudge factor to give advantage to a decision. (default 1).")]
        public float Weight = 1f;

        [Tooltip("Unchecking this can disable the decision (useful for debugging) (default true).")]
        public bool IsEnabled = true;

        [Tooltip("A list of all the considerations that factor into the decision.")]
        [EnumSelectAttribute("Considerations")]
        public List<int> Considerations;
    }
}