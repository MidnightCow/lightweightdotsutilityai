using UnityEngine;
using System.Collections.Generic;

namespace Lwduai
{
    [System.Serializable]
    public class AiProfileEntry : AiTableRecordEntry
    {
        [Tooltip("A list of decisions that this profile will evaluate when choosing an action.")]
        [EnumSelectAttribute("Decisions")]
        public List<int> Decisions;
    }
}