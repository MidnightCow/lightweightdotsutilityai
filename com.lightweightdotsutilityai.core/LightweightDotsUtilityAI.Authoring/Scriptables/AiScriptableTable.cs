﻿using System.Collections.Generic;
using UnityEngine;

namespace Lwduai
{
    [CreateAssetMenu(fileName = "New AI Scriptable Table", menuName = "LightweightDotsUtilityAI/AI Scriptable Table")]
    public class AiScriptableTable : ScriptableObject
    {
        public void UpdateEnums() => AiDataUtils.ParseAllUserData();
        public List<AiInputEntry> Inputs;
        public List<AiConsiderationEntry> Considerations;
        public List<AiDecisionEntry> Decisions;
        public List<AiProfileEntry> Profiles;

        //public void OnValidate()
        //{
        //    for (int i = 0; i < Considerations.Count; i++)
        //    {
        //        Considerations[i].RefreshCurveDisplay();
        //    }
        //}
    }
}
