// This has a bunch of functions for generating enums in the editor.

using System.IO;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Lwduai
{
	public static class AiDataUtils
	{
        public const string DataFolderPath = "Assets/Scripts/LightweightDotsUtilityAIData/";

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////// MAIN GATHER/PARSE FUNCTIONS ////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // THIS IS THE MAIN PUBLIC STATIC FUNCTION TO CALL.
        // GATHERS ALL SCRIPTABLE TABLE ASSETS IN PROJECT,
        // OR CREATES ONE IF NONE EXIST.
        // IT IGNORES ALL EXCEPT THE FIRST SCRIPTABLE FOUND.
        // THEN PARSES ALL ENUMS AND WRITES ENUM DATA FILES.
        // ALSO CREATES ASSEMBLY ASMDEF IN THE DATA FOLDER.
        // ( ASSEMBLY DEFINITION IS ESSENTIAL, AS THIS
        // LightweightDotsUtilityAi.Enums ASSEMBLY IS LOOKED
        // FOR BY THE OTHER LwDUAI ASSEMBLY'S )
        public static void ParseAllUserData()
        {
            UnityEngine.Debug.Log("Finding and parsing all scriptable ai tables in project assets..");

            CleanupDataFilesAndFolders(DataFolderPath);

            Debug.Log("Using data folder path - " + DataFolderPath);

            var inputsList = new List<string>();
            var considerationsList = new List<string>();
            var decisionsList = new List<string>();
            var profilesList = new List<string>();

            var allScriptableTables = GetAllScriptableTables(DataFolderPath);
            var scriptableTable = allScriptableTables[0]; // JUST USE THE FIRST ONE FOUND

            foreach(var item in scriptableTable.Inputs) inputsList.Add(item.Name);
            foreach(var item in scriptableTable.Considerations) considerationsList.Add(item.Name);
            foreach(var item in scriptableTable.Decisions) decisionsList.Add(item.Name);
            foreach(var item in scriptableTable.Profiles) profilesList.Add(item.Name);

            WriteEnums("AiEnumData_Inputs.cs",         MakeEnumDeclaration("AiInputType", inputsList, "ushort", true), true, DataFolderPath);
            WriteEnums("AiEnumData_Considerations.cs", MakeEnumDeclaration("AiConsiderationType", considerationsList, "ushort", true), true, DataFolderPath);
            WriteEnums("AiEnumData_Decisions.cs",      MakeEnumDeclaration("AiDecisionType", decisionsList, "ushort", true), true, DataFolderPath);
            WriteEnums("AiEnumData_Profiles.cs",       MakeEnumDeclaration("AiProfileType", profilesList, "ushort", true), true, DataFolderPath);

            WriteAsmdef("LightweightDotsUtilityAI.Enums.asmdef", true, DataFolderPath);

            AddDefineSymbol();

            UnityEngine.Debug.Log("Finding and parsing complete.");
        }

        // ADDS THE 'LIGHTWEIGHTDOTSUTILITYAI_ENUMS' DEFINE SYMBOL TO THE PROJECT
        // ( Edit->ProjectSettings->Player->ScriptingDefineSymbols )
        // THIS IS IMPORTANT BECAUSE IT TELLS THE LwDUAI SCRIPTS THAT THE ENUM
        // DATA FILES ARE AVAILABLE AND ALLOWS ALL CODEPATHS TO COMPILE
        private static void AddDefineSymbol()
        {
            string currentDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
            HashSet<string> defines = new HashSet<string>(currentDefines.Split(';')) { "LIGHTWEIGHTDOTSUTILITYAI_ENUMS" };

            string newDefines = string.Join(";", defines);
            if (newDefines != currentDefines) PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, newDefines);

            //PlayerSettings.SetScriptingDefineSymbols(EditorUserBuildSettings.selectedStandaloneTarget, newDefines);

            UnityEngine.Debug.Log("Added global define LIGHTWEIGHTDOTSUTILITYAI_ENUMS");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////// MAIN FOLDER MANAGEMENT FUNCTIONS ////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // THERE SHOULD ONLY EVER BE ONE COPY OF EACH OF THE CORE ENUM DATA FILES
        // IN THE PROJECT, SO THIS FUNCTION DESTROYS ANY ADDITIONAL COPIES OUTWITH
        // THE DATA FOLDER PATH ( AND THE PARENT FOLDERS IF NO OTHER NON-ENUM FILES
        // EXIST IN THE FOLDER ).
        // IT ALSO CREATES A FOLDER AT THE DATA PATH IF NONE EXISTS.
        private static void CleanupDataFilesAndFolders(string dataFolderPath)
        {
            var folders = new List<string>();
            var filenames = new List<string>()
            {
                "AiEnums.cs", // THE OLD ENUM FILENAME
                "LightweightDotsUtilityAI.Enums.asmdef",
                "AiEnumData_Inputs.cs",
                "AiEnumData_Considerations.cs",
                "AiEnumData_Decisions.cs",
                "AiEnumData_Profiles.cs"
            };

            if (!Directory.Exists(dataFolderPath))
            {
                Debug.Log("Creating data folder at - " + dataFolderPath);
                Directory.CreateDirectory(dataFolderPath);
            }

            foreach (var fileName in filenames) GetDataFolders(fileName, ref folders, dataFolderPath);

            var deleteCount = 0;

            for (int i = 0; i < folders.Count; i++)
            {
                Debug.Log("Existing data found outside of data folder at - " + folders[i]);
                deleteCount += DestroyExtraFilesFolders(folders[i], filenames); // destroy all folders and contents ( except user content if exists )
            }

            deleteCount += DeleteIfContains("AiEnums.cs", dataFolderPath); // delete the old AiEnums.cs file if exists

            if (deleteCount > 0) AssetDatabase.Refresh();
        }

        // GETS A LIST OF ALL FOLDERS OUTWITH dataFolderPath CONTAINING ANY DATA FILES
        private static void GetDataFolders(string fileName, ref List<string> folders, string dataFolderPath)
        {
            var extension = fileName.Contains(".cs") ? ".cs" : fileName.Contains(".asmdef") ? ".asmdef" : "";
            var assetName = fileName.Replace(extension, ""); // Asset name is filename without the extension

            foreach (var assetID in AssetDatabase.FindAssets(assetName))
            {
                var assetPath = AssetDatabase.GUIDToAssetPath(assetID);
                if (assetPath.EndsWith(fileName))
                {
                    var dataFolder = assetPath.Replace(fileName, "");
                    if (dataFolder != dataFolderPath)
                    {
                        if (!folders.Contains(dataFolder)) folders.Add(dataFolder);
                    }
                }
            }
        }
        private static int DestroyExtraFilesFolders(string folderPath, List<string> filenamesToDestroy)
        {
            var deleteCount = 0;

            foreach (var filenameToDestroy in filenamesToDestroy)
            {
                deleteCount += DeleteIfContains(filenameToDestroy, folderPath);
            }

            var remainingFilesCount = Directory.GetDirectories(folderPath).Length + Directory.GetFiles(folderPath).Length;
            if (remainingFilesCount == 0)
            {
                var folderName = System.IO.Path.GetDirectoryName(folderPath);
                folderName = folderName.Substring(folderName.LastIndexOf("\\")+1);
                var parentDir = folderPath.Replace(folderName + "/", "");
                var folderMetaPath = parentDir + folderName + ".meta";

                deleteCount++;

                Directory.Delete(folderPath, true);
                File.Delete(folderMetaPath);
                Debug.Log("Deleting folder " + folderPath);
            }

            return deleteCount;
        }
        private static int DeleteIfContains(string filenameToDelete, string folderPath)
        {
            var deleteCount = 0;
            foreach (var filepath in Directory.EnumerateFiles(folderPath))
            {
                var file = filepath.Replace(folderPath, "");
                if (file == filenameToDelete)
                {
                    File.Delete(filepath);
                    File.Delete(filepath+".meta");
                    Debug.Log("Deleting file " + filepath);
                    deleteCount++;
                }
            }
            return deleteCount;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////// MAIN SCRIPTABLE ASSET FUNCTIONS ////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // GATHERS ALL AI TABLE SCRIPTABLE TABLE ASSETS IN THE PROJECT
        // AND CREATES ONE IF NONE EXIST.
        private static List<AiScriptableTable> GetAllScriptableTables(string dataFolderPath)
        {
            var tableExists = false;

            var scriptableTableIds = AssetDatabase.FindAssets("t:AiScriptableTable", null);
            var scriptableTables = new List<AiScriptableTable>();

            foreach (var tableID in scriptableTableIds)
            {
                UnityEngine.Debug.Log($"Scriptable ai table found = {AssetDatabase.GUIDToAssetPath(tableID)}");

                var scriptableTable = (AiScriptableTable)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(tableID), typeof(AiScriptableTable));
                scriptableTables.Add(scriptableTable);
                tableExists = true;
            }

            if (!tableExists) scriptableTables.Add(CreateScriptableAsset(dataFolderPath, "NewAiScriptableTable"));

            return scriptableTables;
        }

        // CREATES SCRIPTABLE AI TABLE ASSET AND SAVES TO ASSETS FOLDER
        private static AiScriptableTable CreateScriptableAsset(string folderPath, string fileName)
        {
            UnityEngine.Debug.Log("Trying to create new Scriptable AiTable asset - " + fileName + " at " + folderPath);

            var asset = ScriptableObject.CreateInstance<AiScriptableTable>();

            var fullPath = AssetDatabase.GenerateUniqueAssetPath(folderPath + fileName + ".asset");
            AssetDatabase.CreateAsset(asset, fullPath);
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;

            return asset;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////// MAIN WRITE TO DISK FUNCTIONS ////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private static void WriteEnums(string fileName, string declaration, bool reImport, string folderPath)
		{
            var text =
				"// This file was auto-generated\n\n" +
				"namespace Lwduai\n" +
				"{\n" +
				declaration + "\n" +
				"}";

            WriteToFile(fileName, folderPath, text, reImport);
        }
        private static void WriteAsmdef(string fileName, bool reImport, string folderPath)
        {
            var text = "{\r\n\t\"name\": \"LightweightDotsUtilityAI.Enums\"\r\n}\r\n";

			WriteToFile(fileName, folderPath, text, reImport);
        }

        private static void WriteToFile(string fileName, string folderPath, string text, bool reImport)
        {
            UnityEngine.Debug.Log($"Writing file - {folderPath}/{fileName}");

            var fullAssetPath = System.IO.Path.Combine(folderPath, fileName);

            // ensure that the output directory exists
            System.IO.Directory.CreateDirectory(folderPath);
            System.IO.File.WriteAllText(fullAssetPath, text);
            if (reImport) ImportAsset(fullAssetPath);
        }
        private static void ImportAsset(string fullAssetPath) 
		{
            AssetDatabase.ImportAsset(fullAssetPath);
            AssetDatabase.LoadMainAssetAtPath(fullAssetPath);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////// PARSE TO STRING FUNCTIONS ////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private static readonly HashSet<string> m_keywords = new HashSet<string> {
            "abstract", "as", "base", "bool", "break", "byte", "case", "catch", "char", "checked",
            "class", "const", "continue", "decimal", "default", "delegate", "do", "double", "else",
            "enum", "event", "explicit", "extern", "false", "finally", "fixed", "float", "for",
            "foreach", "goto", "if", "implicit", "in", "int", "interface", "internal", "is", "lock",
            "long", "namespace", "new", "null", "object", "operator", "out", "override", "params",
            "private", "protected", "public", "readonly", "ref", "return", "sbyte", "sealed",
            "short", "sizeof", "stackalloc", "static", "string", "struct", "switch", "this", "throw",
            "true", "try", "typeof", "uint", "ulong", "unchecked", "unsafe", "ushort", "using",
            "virtual", "void", "volatile", "while"
        };

        // This function will return a string containing an enum declaration with the specified parameters.
        // name --> the name of the enum to create
        // values -> the enum values
        // primitive --> byte, int, uint, short, int64, etc (empty string means no type specifier)
        // makeClassSize --> if this is true, an extra line will be added that makes a static class to hold the size.
        // example:
        //    print(MakeEnumDeclaration("MyType", { Option1, Option2, Option3 }, "byte", true));
        //    output -->  public enum MyType : byte { Option1, Option2, Option3 }
        //                public static class MyTypeSize { public const byte Size = 3; }
        private static string MakeEnumDeclaration(string name, List<string> values, string primitive, bool makeSizeClass)
		{
			string prim = primitive.Length <= 0 ? "" : " : " + primitive;
			string declaration = "\tpublic enum " + name + prim + " { ";
			int countMinusOne = values.Count - 1;
			for (int i = 0; i < values.Count; i++) {
				declaration += MakeStringEnumCompatible(values[i]);
				if (i < countMinusOne) { declaration += ", "; }
			}
			declaration += " }\n";
			if (makeSizeClass) {
				declaration += $"\tpublic static class {name}Size {{ public const {primitive} Size = {values.Count}; }}\n";
			}
			return declaration;
		}

        // given a string, attempts to make the string compatible with an enum
        // if there are any spaces, it will attempt to make the string camel-case
        private static string MakeStringEnumCompatible(string text)
		{
			if (text.Length <= 0) { return "INVALID_ENUM_NAME"; }
			string ret = "";

			// first char must be a letter or an underscore, so ignore anything that is not
			if (char.IsLetter(text[0]) || (text[0] == '_')) { ret += text[0]; }

			// ignore anything that's not a digit or underscore
			bool enableCapitalizeNextLetter = false;
			for (int i = 1; i < text.Length; ++i) {
				if (char.IsLetterOrDigit(text[i]) || (text[i] == '_')) {
					if (enableCapitalizeNextLetter) {
						ret += char.ToUpper(text[i]);
					} else {
						ret += text[i];
					}
					enableCapitalizeNextLetter = false;
				} else if (char.IsWhiteSpace(text[i])) {
					enableCapitalizeNextLetter = true;
				}
			}
			if (ret.Length <= 0) { return "INVALID_ENUM_NAME"; }

			// all the keywords are lowercase, so if we just change the first letter to uppercase,
			// then there will be no conflict
			if (m_keywords.Contains(ret)) { ret = char.ToUpper(ret[0]) + ret.Substring(1); }

			return ret;
		}
    }
}