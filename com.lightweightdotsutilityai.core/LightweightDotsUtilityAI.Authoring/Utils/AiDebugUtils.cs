﻿using System;
using System.Collections.Generic;
using Unity.Entities;

namespace Lwduai
{
    public static class AiDebugUtils
    {
#if LIGHTWEIGHTDOTSUTILITYAI_ENUMS
        // return a string containing debugging info for the specified entity.
        public static string MakeAiDetailString(EntityManager em, BlobAssetReference<AiTableDef> table, Entity entity)
        {
            DynamicBuffer<AiDecisionScoreData> decisionScores = em.GetBuffer<AiDecisionScoreData>(entity);
            DynamicBuffer<AiConsiderationScoreData> considerationScores = em.GetBuffer<AiConsiderationScoreData>(entity);
            DynamicBuffer<AiInputData> inputs = em.GetBuffer<AiInputData>(entity);
            AiActionData winner = em.GetComponentData<AiActionData>(entity);

            if ((ushort)winner.Score != ushort.MaxValue)
            {
                string text = "";
                // each entity has one score value per decision
                for (ushort d = 0; d < decisionScores.Length; d++)
                {
                    AiDecisionScoreData decisionScore = decisionScores[d];
                    if (decisionScore.IsUsedByProfile)
                    { // ignore actions not used by this profile
                        text += $"  {(AiDecisionType)d} --> {decisionScore.Score}\n";
                        // In order to get the scores for each agent's consideration, we need to find out which considerations this decision has.
                        // First, fetch the definition for the current decision.
                        ref AiDecisionDef decisionDefinition = ref table.Value.Decisions[d];
                        // for every consideration that this decision has...
                        for (ushort c = 0; c < decisionDefinition.ConsiderationIds.Length; c++)
                        {
                            AiConsiderationType considerationId = decisionDefinition.ConsiderationIds[c];
                            AiInputType inputId = table.Value.Considerations[(ushort)considerationId].InputId;
                            text += $"    {considerationId} --> {considerationScores[(ushort)considerationId].Score} ({inputId} : {inputs[(ushort)inputId].Value})\n";
                        }
                    }
                }
                return $"[{entity.Index} : {winner.Action}, {winner.Score}]\n{text}";
            }
            return $"[{entity.Index} : No Winning Action]";
        }

        // Returns a simple string containing health data on the agent
        // H (health) is sorta game dependent.
        public static string MakeAiQuickInfoString(EntityManager em, BlobAssetReference<AiTableDef> table, Entity entity, short healthInputIndex)
        {
            AiActionData winner = em.GetComponentData<AiActionData>(entity);
            DynamicBuffer<AiInputData> inputs = em.GetBuffer<AiInputData>(entity);
            float health = inputs[healthInputIndex].Value;
            return $"{entity.Index} : {winner.Action}, S: {winner.Score} H:{health}";
        }

        // This function attempts to get a table reference from the default world.
        // If it fails, a default reference will be returned, which can be checked against IsCreated to determine success or failure.
        public static BlobAssetReference<AiTableDef> TryGetDwAiTableReference()
        {
            if (!World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(ComponentType.ReadOnly<AiTableData>()).TryGetSingleton(out AiTableData table)) { return default; }
            return table.Reference;
        }

        // This function fetches a definition reference from the default world.
        public static BlobAssetReference<AiTableDef> GetDwAiTableReference()
        {
            return World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(ComponentType.ReadOnly<AiTableData>()).GetSingleton<AiTableData>().Reference;
        }
#endif
    }
}
