// This singleton class draws a gizmo that displays debugging information next to an agent
// that helps you figure out why an agent has chosen a particular action (editor only).
// If you check the checkbox for enable detail, then the scores for each agent will display next to the agent.
//-----------------------------------------------------------------------------------------------------------------------//

using UnityEditor;
using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using Unity.Transforms;
using Unity.Mathematics;

namespace Lwduai
{
    public enum AiTextMode { Detail, QuickInfo }

    public class AiTextGizmo : Singleton<AiTextGizmo>
    {
        [Tooltip("Enable/Disable AI debug output (default true)")] public AiTextMode m_textMode;
        [Tooltip("An integer number for font size that scales with screen size, 1..n (default 2)")] public int m_fontSize = 2;
        [Tooltip("Color of the text to use (default null)")] public Color m_textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        [Tooltip("X Offset (default 0)")] public int m_xOffset = 0;
        [Tooltip("Y Offset (default 0)")] public int m_yOffset = 0;

        // If you don't have a health input on your agent, you can igore the H (or use it to display whatever input you want to show)
        // This index should correspond to a value in the AiInputType enum.
        [Tooltip("Index of the health input (used with quick-info)")] public short m_healthInputIndex = 0;

        public AiTextMode GetTextMode() { return m_textMode; }

#if LIGHTWEIGHTDOTSUTILITYAI_ENUMS
        BlobAssetReference<AiTableDef> m_table;

        void OnDrawGizmos()
        {
            if (!Application.isPlaying) { return; }
            if (!m_table.IsCreated) {
                m_table = AiDebugUtils.TryGetDwAiTableReference();
                if (!m_table.IsCreated) { return; }
            }

            GUIStyle style = new GUIStyle();
            style.alignment = TextAnchor.UpperLeft;
            style.fontSize = (Screen.height * m_fontSize) / 200;
            style.normal.textColor = m_textColor;

            EntityManager em = World.DefaultGameObjectInjectionWorld.EntityManager;
            EntityQuery query = em.CreateEntityQuery(ComponentType.ReadOnly<AiActionData>(), ComponentType.ReadOnly<LocalTransform>());
            NativeArray<LocalTransform> xforms = query.ToComponentDataArray<LocalTransform>(Allocator.Temp);
            NativeArray<Entity> entities = query.ToEntityArray(Allocator.Temp);
            query.CompleteDependency();


            // for every agent that was found
            for (int e = 0; e < entities.Length; e++) {
                if (m_textMode == AiTextMode.Detail) {
                    Handles.Label(xforms[e].Position + math.up() * 0.125f, AiDebugUtils.MakeAiDetailString(em, m_table, entities[e]), style);
                } else if (m_textMode == AiTextMode.QuickInfo) {
                    Handles.Label(xforms[e].Position + math.up() * 0.125f, AiDebugUtils.MakeAiQuickInfoString(em, m_table, entities[e], m_healthInputIndex), style);
                }
            }
        }
#endif
    }
} // namespace