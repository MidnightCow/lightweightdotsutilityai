// This file contains several systems for executing scoring and presenting the results.
//
// The general order is:
//   First Frame (AiFirstFrameTag)
//      1) The score kickoff system runs (first thing in early sim group).
//      2) One or more cheap input job(s) run (anytime in early sim group).
//      3) An early decision job calculates scores with the cheap-inputs (last thing in early sim group).
//      4) Optional - Expensive input jobs run (anytime in simulation group).
//      5) Optional - The Auto-score job runs at end of simulation group.
//   Frames 1.. N-1
//      1) Optional - More expensive input jobs run if configured to do so.
//      2) Optional - Auto-score job runs at end of simulation group (depends on user config).
//   Frame N-1
//      1) A Job copies the final winner into AiActionData (end of frame N-1).
//   Last Frame (AiLastFrameTag)
//      1) Reaction jobs run (anytime in the last frame before the end simulation group).
//      2) Actions are reset at the end (late sim group of frame N).
//--------------------------------------------------------------------------------------------//


#if LIGHTWEIGHTDOTSUTILITYAI_ENUMS

using Unity.Entities;
using Unity.Burst;
using Unity.Collections;

namespace Lwduai
{

    //#####################################################################################//
    //############################### EARLY SIM GROUP #####################################//
    //#####################################################################################//

    // This job initializes values and performs a first-pass scoring.
    [BurstCompile]
    public partial struct AiEarlyScoreJob : IJobEntity
    {
        // executes for all agents
        public void Execute(AiAgentAspect ai)
        {
            ai.Winner = new AiWinnerData { Index = -1, Score = float.MinValue };
            ai.RunInitialScore();
        }
    }

    [BurstCompile]
    public partial struct AiResetJob : IJobEntity
    {
        public void Execute(AiAgentAspect ai)
        {
            ai.ResetInputs();
            ai.ResetConsiderations();
            ai.ResetDecisions();
        }
    }

    // This system runs at the very beginning of every frame.
    // This system is responsible for incrementing the frame number and kicking off the scoring process.
    [UpdateInGroup(typeof(EarlySimulationSystemGroup), OrderFirst = true)]
    [BurstCompile]
    public partial struct AiScoreKickoffSystem : ISystem
    {
        UpdateTimer m_timer;

        void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<AiScoreConfigData>();
            state.RequireForUpdate<AiFrameStateData>();

            // 999 means interval is not set
            // It will be set in OnUpdate because it requires AiScoreConfigData.
            m_timer = new UpdateTimer(999);
        }

        [BurstCompile]
        void OnUpdate(ref SystemState state)
        {
            sbyte oldFrameNumber = SystemAPI.GetSingleton<AiFrameStateData>().FrameNumber;

            AiScoreConfigData cfg = SystemAPI.GetSingleton<AiScoreConfigData>();
            Entity singletonEntity = SystemAPI.GetSingletonEntity<AiFrameStateData>();
            if (m_timer.GetInterval() == 999) { m_timer.SetInterval(cfg.ScoringInterval); }

            sbyte newFrameNumber = oldFrameNumber;
            if (oldFrameNumber >= 0) { // only tick frames 1..N..-1 when the interval has passed
                if (oldFrameNumber < cfg.LastFrame) {
                    newFrameNumber = (sbyte)(oldFrameNumber + 1);
                } else if (oldFrameNumber >= cfg.LastFrame) {
                    newFrameNumber = -1;
                }
                SystemAPI.SetSingleton(new AiFrameStateData { FrameNumber = newFrameNumber });
                AiUtils.CycleFrameComponent(newFrameNumber, cfg.LastFrame, ref state, singletonEntity);
            }

            if (m_timer.IsNotReady(SystemAPI.Time.DeltaTime)) { return; }

            // code only reaches here every interval.

            // start a new frame cycle
            if (oldFrameNumber < 0) {
                // sets off a reaction where frame number increments every frame
                SystemAPI.SetSingleton(new AiFrameStateData { FrameNumber = 0 });
                AiUtils.CycleFrameComponent(0, cfg.LastFrame, ref state, singletonEntity);

                AiResetJob job = new AiResetJob();
                state.Dependency = job.ScheduleParallel(state.Dependency);
            }
        }
    }

    // This system runs at the beginning of frame 0 only, but after all the cheap input jobs finish.
    // It launches a job that initializes values and performs a first-pass scoring.
    [BurstCompile]
    [RequireMatchingQueriesForUpdate]
    [UpdateInGroup(typeof(EarlySimulationSystemGroup), OrderLast = true)]
    public partial struct AiEarlySystem : ISystem
    {
        void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<AiFrameStateData>();
            state.RequireForUpdate<AiFirstFrameTag>();
        }

        [BurstCompile]
        void OnUpdate(ref SystemState state)
        {
            // IsFresh will be set to true by this job, and then every system after this one can read it in this frame.
            // When the next frame comes around, the action will be reset.
            AiEarlyScoreJob job = new AiEarlyScoreJob();
            state.Dependency = job.ScheduleParallel(state.Dependency);
        }
    }


    //#####################################################################################//
    //############################### LATE SIM GROUP ######################################//
    //#####################################################################################//

    // This job updates the action for all entities using the winner component.
    [BurstCompile]
    public partial struct AiActionSetJob : IJobEntity
    {
        public void Execute(ref AiActionData action, in AiWinnerData winner)
        {
            action.OldAction = action.Action;
            action.Action = (AiDecisionType)winner.Index;
            action.Score = winner.Score;
        }
    }

    // If the user selected the option to run it, this job will run after every frame
    // and it will score all incomplete decisions.
    [BurstCompile]
    public partial struct AiAutoEndFrameScoreJob : IJobEntity
    {
        public void Execute(AiAgentAspect ai)
        {
            ai.RunPartialScore();
        }
    }

    // This system only runs at the very end of an AI calculation frame (final sweep).
    // It has two jobs:
    //    In frame N-1 it launches a job to do the final scoring (the action component is set).
    //    In frame N a job launches to reset frame number to -1 and set each actions is-fresh flag to false.
    [UpdateInGroup(typeof(LateSimulationSystemGroup), OrderFirst = true)]
    public partial struct AiLateSystem : ISystem
    {
        void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<AiFrameStateData>();
            state.RequireForUpdate<AiScoreConfigData>();
        }

        [BurstCompile]
        void OnUpdate(ref SystemState state)
        {
            sbyte frameNumber = SystemAPI.GetSingleton<AiFrameStateData>().FrameNumber;
            AiScoreConfigData cfg = SystemAPI.GetSingleton<AiScoreConfigData>();

            // if the auto-score option is set, run the auto score on every frame except for the last one
            if (cfg.EnableAutoEndFrameScore && ((frameNumber >= 0) && (frameNumber < cfg.LastFrame))) {
                // This runs for frame 0... N-1
                AiAutoEndFrameScoreJob job = new AiAutoEndFrameScoreJob();
                state.Dependency = job.ScheduleParallel(state.Dependency);
            }

            if (frameNumber == (cfg.LastFrame - 1)) {
                // next to last frame (set AiActionData)
                AiActionSetJob job = new AiActionSetJob();
                state.Dependency = job.Schedule(state.Dependency);
            }
        }
    }

    //#####################################################################################//
    //############################### INITIALIZATION ######################################//
    //#####################################################################################//

    // This system will run only once when an agent is first created.
    // It will add the AiAgentTableData to the agent and set its table reference.
    [RequireMatchingQueriesForUpdate]
    [BurstCompile]
    public partial struct AiInitAgentSystem : ISystem
    {
        EntityQuery m_allAgentsQuery;

        void OnCreate(ref SystemState state)
        {
            m_allAgentsQuery = SystemAPI.QueryBuilder().WithAll<AiActionData>().WithNone<AiAgentTableData>().Build();
            state.RequireForUpdate<AiTableData>();
        }

        [BurstCompile]
        void OnUpdate(ref SystemState state)
        {
            AiTableData Table = SystemAPI.GetSingleton<AiTableData>();

            var entities = m_allAgentsQuery.ToEntityArray(Allocator.Temp);
            foreach (var entity in entities) {
                state.EntityManager.AddComponentData(entity, new AiAgentTableData { Reference = Table.Reference });
            }
        }
    }
} // namespace

#endif