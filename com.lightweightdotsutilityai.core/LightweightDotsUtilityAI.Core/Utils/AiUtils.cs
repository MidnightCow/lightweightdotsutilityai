// holds various data structures and functions for Utility AI
// (mainly building the blob asset and scoring)
//-----------------------------------------------------------//


#if LIGHTWEIGHTDOTSUTILITYAI_ENUMS

using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace Lwduai
{
    // ##################### AITABLE STATIC CLASS ################## //

    public static class AiUtils
    {
        // returns val scaled between 0 and 1
        // if val is smaller than oldMin it will be clamped to oldMin
        // if val is larger than oldMax it will be clamped to oldMax
        public static float ScaleToClampedUnitInterval(float val, float oldMin, float oldMax)
        {
            return (math.clamp(val, oldMin, oldMax) - oldMin) / (oldMax - oldMin);
        }

        // Teturn a hashset containing all of the decisions.
        // Note - I'm not using this right now...
        // I think it's faster to just loop over each decision and skip the ones not used by the specified profiles.
        // The allocation is probably slower.
        public static NativeHashSet<ushort> MakeDecisionHashSet(in DynamicBuffer<AiProfileData> profiles, Allocator allocator, in AiAgentAspect ai)
        {
            // first get a set of all decisions used by the profile.
            NativeHashSet<ushort> mergedDecisionIds = new NativeHashSet<ushort>(0, allocator);
            for (ushort p = 0; p < profiles.Length; p++) {
                ref AiProfileDef profileDef = ref ai.DefTable.Profiles[(ushort)profiles[p].ProfileId];
                // for every decision in the profile...
                for (ushort d = 0; d < profileDef.DecisionIds.Length; d++) {
                    mergedDecisionIds.Add((ushort)profileDef.DecisionIds[d]);
                }
            }
            return mergedDecisionIds;
        }

        // This function will add/remove frame number tags to the specified entity based on the specified frame.
        // It will remove the previous tag and add a new tag.
        public static void CycleFrameComponent(sbyte frameNumber, sbyte lastFrame, ref SystemState state, Entity entity)
        {
            if (frameNumber == lastFrame) { state.EntityManager.AddComponent<AiLastFrameTag>(entity); }

            // at 30fps with 7 frames total
            //  1   2   3  4  5  6  7      1   2   3  4  5  6  7      1   2   1
            // -1, -1, -1, 0, 1, 2, 3 LF, -1, -1, -1, 0, 1, 2, 3 LF, -1, -1, -1...

            switch (frameNumber) {
                case -1:
                    if (lastFrame == 9) { state.EntityManager.RemoveComponent<AiFrame9Tag>(entity); }
                    state.EntityManager.RemoveComponent<AiLastFrameTag>(entity);
                    EnableFrameComponent(lastFrame, ref state, entity, false);
                    break;
                case 0:
                    state.EntityManager.AddComponent<AiFirstFrameTag>(entity);
                    break;
                case 1:
                    state.EntityManager.RemoveComponent<AiFirstFrameTag>(entity);
                    state.EntityManager.AddComponent<AiFrame1Tag>(entity);
                    break;
                case 2:
                    state.EntityManager.RemoveComponent<AiFrame1Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame2Tag>(entity);
                    break;
                case 3:
                    state.EntityManager.RemoveComponent<AiFrame2Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame3Tag>(entity);
                    break;
                case 4:
                    state.EntityManager.RemoveComponent<AiFrame3Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame4Tag>(entity);
                    break;
                case 5:
                    state.EntityManager.RemoveComponent<AiFrame4Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame5Tag>(entity);
                    break;
                case 6:
                    state.EntityManager.RemoveComponent<AiFrame5Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame6Tag>(entity);
                    break;
                case 7:
                    state.EntityManager.RemoveComponent<AiFrame6Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame7Tag>(entity);
                    break;
                case 8:
                    state.EntityManager.RemoveComponent<AiFrame7Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame8Tag>(entity);
                    break;
                case 9:
                    state.EntityManager.RemoveComponent<AiFrame8Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame9Tag>(entity);
                    break;
            }
        }

        // adds or removes a frame tag based on the specified frame number and toggle mode
        public static bool EnableFrameComponent(sbyte frameNumber, ref SystemState state, Entity entity, bool enable)
        {
            switch (frameNumber) {
                case 0: return enable ? state.EntityManager.AddComponent<AiFirstFrameTag>(entity) : state.EntityManager.RemoveComponent<AiFirstFrameTag>(entity);
                case 1: return enable ? state.EntityManager.AddComponent<AiFrame1Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame1Tag>(entity);
                case 2: return enable ? state.EntityManager.AddComponent<AiFrame2Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame2Tag>(entity);
                case 3: return enable ? state.EntityManager.AddComponent<AiFrame3Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame3Tag>(entity);
                case 4: return enable ? state.EntityManager.AddComponent<AiFrame4Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame4Tag>(entity);
                case 5: return enable ? state.EntityManager.AddComponent<AiFrame5Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame5Tag>(entity);
                case 6: return enable ? state.EntityManager.AddComponent<AiFrame6Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame6Tag>(entity);
                case 7: return enable ? state.EntityManager.AddComponent<AiFrame7Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame7Tag>(entity);
                case 8: return enable ? state.EntityManager.AddComponent<AiFrame8Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame8Tag>(entity);
                case 9: return enable ? state.EntityManager.AddComponent<AiFrame9Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame9Tag>(entity);
            }
            return false;
        }
    }
} // namespace

#endif