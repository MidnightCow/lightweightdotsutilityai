// This static utility class contains functions for evaluating response curves

// common preset cheat-sheet
// u-parabola - Quadratic --> m = 4, k = 2/4/6/8/etc, b = 0, c = 0.5
// n-parabola - Quadratic --> m = -4, k = 2/4/6/8/etc, b = 1, c = 0.5
// n-normal - Normal --> m = 1, k = 0.5, b = 0, c = 0
// u-normal - Normal --> m = -1, k = 0.5, b = 1, c = 0
// s-forwards - Logistic --> m = 10, k = 1, b = 0, c = 0.5
// s-backwards - Logistic --> m = 10, k = -1, b = 1, c = 0.5
// s-sideways - Logit --> m = 10, k = -4, b = -2, c = 0
// s-cw90-flat-middle - quadratic --> m = -16, k = 5, b = 0.5, c = 0.5
// s-backwards-cw90-flat-middle - quadratic --> m = 16, k = 5, b = 0.5, c = 0.5
// s-backwards-sideways - Logit --> m = 10, k = 4, b = 2, c = 0
// v-parabola - Angle --> m = 2, k = 0, b = 0, c = 0.5
// ^-parabola - Angle --> m = -2, k = 0, b = 1, c = 0.5
// s-binary - Threshold --> m = 0.5, k = 1 (if x > m, returns 1)
// s-backwards-binary - Threshold --> m = 0.5, k = -1 (if x > m, returns 0)
// /-line - Linear --> m = 1, b = 1, c = 1
// \-line - Linear --> m = -1, b = 1, c = 0
// sine - Sine --> m = 1, b = 0, c = -1, k = 1

// logistic --> https://www.desmos.com/calculator/7mmudjzoef
// angle --> https://www.desmos.com/calculator/v9ikn34igj
// linear --> https://www.desmos.com/calculator/abguth4se8
// quadratic --> https://www.desmos.com/calculator/xsstfzswno
// normal --> https://www.desmos.com/calculator/as7cpwot3i
// sine --> https://www.desmos.com/calculator/0m6hfaiozz
// angle --> https://www.desmos.com/calculator/v9ikn34igj
// logit --> https://www.desmos.com/calculator/booioznprb
// step --> https://www.desmos.com/calculator/mkwvtxs589
// square --> https://www.desmos.com/calculator/7pmqxuuaqj
// decay --> https://www.desmos.com/calculator/vnxpawj616
// decayvariant --> https://www.desmos.com/calculator/x5ld4zetut

// the square wave has an extra parameter "s" for how square the corners are, but since there are only four vars to work with (m,k,b,c), s is hardcoded to 100.


using Unity.Mathematics;

namespace Lwduai
{
    public enum CurveType : byte { Linear, Quadratic, Logistic, Logit, Angle, Normal, Sine, Step, Square, Decay, DecayVariant, PassThrough, PassThroughInvert }

    [System.Serializable]
    public struct CurveData
    {
        public CurveType CurveType; // 1 byte
        public float M; // 4 bytes
        public float K; // 4 bytes
        public float B; // 4 bytes
        public float C; // 4 bytes
    } // 17 bytes total

    public class CurveUtils
    {
        // type --> the type of curve (in)
        // x --> the current value (in)
        // m --> slope
        // k --> bend amount, squish amount
        // b --> verticle shift
        // c --> horizontal shift
        // return --> the new value
        public static float Evaluate(float x, CurveType type, float m, float k, float b, float c)
        {
            switch (type) {
                case CurveType.Linear: return math.clamp((m * (x - c)) + b, 0, 1);
                case CurveType.Quadratic: return Sanitize(((m * math.pow(x - c, k)) + b));
                case CurveType.Logistic: return Sanitize((k * (1 / (1 + math.pow(math.pow(math.E, m), -x + c)))) + b);
                case CurveType.Logit: return Sanitize((logn((x + c) / (1 - x - c), m) + b) / k);
                case CurveType.Angle: return math.clamp((m * math.abs(x - c)) + b, 0, 1);
                case CurveType.Normal: return Sanitize(m * math.exp(-30.0f * k * (x - c - 0.5f) * (x - c - 0.5f)) + b);
                case CurveType.Sine: return Sanitize(0.5f * m * math.sin(k * math.PI * ((k * x) - c)) + 0.5f + b);
                case CurveType.Step: return math.clamp((m * math.floor((k * x) - c)) + b, 0, 1);
                case CurveType.Square: float s = 100; return Sanitize(((m / (math.pow(math.abs(s), s * math.sin((k * x) - c)) + 1)) - (m * 0.5f)) + b);
                case CurveType.Decay: return Sanitize((math.pow(m, m * (x - c)) * k) + b);
                case CurveType.DecayVariant: return Sanitize((math.pow(m, (k * x) - c) * k) + b);
                case CurveType.PassThrough: return x; // user is responsible for ensuring value is clamped
                case CurveType.PassThroughInvert: return -x;
            }
            return 0f;
        }

        public static float Evaluate(float x, in CurveData curve)
        {
            return Evaluate(x, curve.CurveType, curve.M, curve.K, curve.B, curve.C);
        }

        // we need this because unlike System.Math.Log(), unity.mathematics doesn't have a version that allows for overloading the base
        protected static float logn(float x, float baseVal) { return math.log2(x) / math.log2(baseVal); }

        // ensures that f is not NAN or infinit and also clamps f to between 0 and 1
        protected static float Sanitize(float f)
        {
            if (float.IsNaN(f)) { return 0f; }
            if (float.IsInfinity(f)) { return 0f; }
            return math.clamp(f, 0, 1);
        }
    }
} // namespace