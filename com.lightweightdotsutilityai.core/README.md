﻿
<div align="center"><h1>Lightweight DOTS Utility AI</h1></div>

<div align="center"><img src="Images/demo2.gif"></div>
<br>
<b>Lightweight DOTS Utility AI</b> is an <b>Infinite Axis Utility AI</b> package that is made specifically for DOTS.
<br><br>
<ul>
<li>Compact and designed so that it doesn't intrude in your code - most of your systems can remain untouched and won't directly need to know about the AI.</li>
<li>Uses a pure Data Orientated approach so that scoring is performed with SIMD and systems act asynchronously.</li>
<li>Profiles, Actions, Considerations and Inputs are referred to by IDs so that the only code generation used is for some simple enums.</li>
<li>Support for skipping input calculations optimization (via the NeedsCalculation flag). <b>*New</b></li>
<li>Support for runtime profile swapping/adding/removing (not tested yet)<b>*New</b></li>
</ul>

<b>Samples</b> available to Import through Unity Package Manager ( samples require <b>Entities Graphics</b> and <b>UniversalRP</b> packages )

Created by <b>LClemens</b> with refactoring work by <b>MidnightCow</b>.