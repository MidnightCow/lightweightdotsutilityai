// This is a quick and dirty (hacky) way to update the agent counts text field in the utility AI example.

using Unity.Entities;
using Unity.Collections;
using TMPro;
using UnityEngine;
using Lwduai;

public partial class AgentCountUpdateSystem : SystemBase
{
    UpdateTimer m_timer = new UpdateTimer(1.0f);
    EntityQuery m_enemyQuery;
    EntityQuery m_playerQuery;

    TextMeshProUGUI m_agentCountsText;
    
    protected override void OnCreate()
    {
        m_enemyQuery = new EntityQueryBuilder(Allocator.Persistent).WithAll<EnemyTag>().Build(this);
        m_playerQuery = new EntityQueryBuilder(Allocator.Persistent).WithAll<PlayerTag>().Build(this);
    }

    protected override void OnUpdate()
    {
        if (m_timer.IsNotReady(SystemAPI.Time.DeltaTime)) { return; }
        if (m_agentCountsText == null) // super hacky as not available during create //
        {
            m_agentCountsText = GameObject.Find("AgentCountsText").GetComponent<TextMeshProUGUI>();
        }

        m_agentCountsText.text = $"Players: {m_playerQuery.CalculateEntityCount()}\nEnemies: {m_enemyQuery.CalculateEntityCount()}";
    }
}