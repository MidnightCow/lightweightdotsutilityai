// Whenever an agent has a valid target, this system will move it towards the target.
// In a real game, this would likely follow a path that was searched via A* or some other algorithm.
//
// One valid way to handle this could have been to read the AiActionData directly and only move then.
// However, often there are systems (third party or just standalone ones) that need to be independent of the AI.
// So this example demonstrates that - an independent movement system that can be reused anywhere.

using Unity.Entities;
using Unity.Burst;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Collections;

[BurstCompile]
public partial struct AgentMovementJob : IJobEntity
{
    [ReadOnly] public ComponentLookup<LocalToWorld> LtwLookup;
    public float DeltaTime;

    [BurstCompile]
    public void Execute(ref LocalTransform lt, in TargetData target, in MoveSpeedData speed)
    {
        if (target.TargetEntity == Entity.Null) { return; }
        if (!LtwLookup.HasComponent(target.TargetEntity)) { return; }

        // fetch the target position
        float3 targetPos = LtwLookup[target.TargetEntity].Position;
        
        // calculate the direction from the current agent's position to the target
        float3 dir = math.normalize(targetPos - lt.Position);

        // Move the entity in the necessary direction by the amount at delta-time multiplied with speed.
        lt.Position += (dir * DeltaTime * speed.Speed);
    }
}

[BurstCompile]
[RequireMatchingQueriesForUpdate]
public partial struct AgentMovementSystem : ISystem
{
    [BurstCompile]
    void OnUpdate(ref SystemState state)
    {
        AgentMovementJob job = new AgentMovementJob();
        job.DeltaTime = SystemAPI.Time.DeltaTime;
        job.LtwLookup = SystemAPI.GetComponentLookup<LocalToWorld>(true);
        job.ScheduleParallel();
    }
}