// This system causes the countdown timer to decrement for every entity that has one.

using Unity.Entities;
using Unity.Burst;

[BurstCompile]
public partial struct AttackCooldownJob : IJobEntity
{
    public float DeltaTime;

    [BurstCompile]
    public void Execute(ref AttackCooldownData cooldown)
    {
        cooldown.Update(DeltaTime);
    }
}

[BurstCompile]
[RequireMatchingQueriesForUpdate]
public partial struct AttackCooldownSystem : ISystem
{
    [BurstCompile]
    void OnUpdate(ref SystemState state)
    {
        AttackCooldownJob job = new AttackCooldownJob();
        job.DeltaTime = SystemAPI.Time.DeltaTime;
        state.Dependency = job.Schedule(state.Dependency);
    }
}