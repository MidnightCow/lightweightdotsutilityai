// This is an example system that is responsible for copying "cheap" input data
// into input values for all enemy entities, (where "cheap" means computationally inexpensive).
//
// NOTE: This does not need to be a single system - you could break it up into multiple systems,
// running at different rates and pulling in different variables.
//
// WARNING: These types of low-cost input should always run in EarlySimulationSystemGroup !!
// WARNING: You should use RequireForUpdate<AiFirstFrameTag>() for these cheap-input systems !!
//
// ScheduleParalle() is used because it happens for every agent.
// It only ever runs in the first frame (AiFirstFrameTag).
//--------------------------------------------------------------------------------------------//

using Unity.Entities;
using Unity.Burst;
using Lwduai;

[BurstCompile]
public partial struct ExampleCheapInputJob : IJobEntity
{
    // Iterates over all translation components and increments upwards movement by one.
    [BurstCompile]
    public void Execute(ref DynamicBuffer<AiInputData> inputs, in HealthData health, in MaxHealthData maxHealth, in AttackCooldownData attackCooldown, in AttackData attack, in HealthInfluenceData healthInfluence)
    {
        inputs[(ushort)AiInputType.MyHealth] = new AiInputData { IsFreshInput = true, RefCount = 0, Value = health.Health / maxHealth.Health };

        // attack cooldown is a number between 0 and 1, where 0.0 means the attack cooldown is ready and 1 means the cooldown just started
        inputs[(ushort)AiInputType.MyAttackCooldown] = new AiInputData { IsFreshInput = true, RefCount = 0, Value = AiUtils.ScaleToClampedUnitInterval(attackCooldown.Time, 0, attackCooldown.Interval) };

        // the agent's attack range (constant in this demo, but it could vary in other games based on buffs, weapons, poisons, etc)
        inputs[(ushort)AiInputType.MyAttackRange] = new AiInputData { IsFreshInput = true, RefCount = 0, Value = attack.Range };

        inputs[(ushort)AiInputType.MyHealAmount] = new AiInputData { IsFreshInput = true, RefCount = 0, Value = healthInfluence.Amount };

        inputs[(ushort)AiInputType.MyDistanceToHealth] = new AiInputData { IsFreshInput = true, RefCount = 0, Value = healthInfluence.DistanceToNearestStation };
    }
}

// Note the system order of this - All cheap input systems should run in the EarlySimulationSystemGroup
[UpdateInGroup(typeof(EarlySimulationSystemGroup))]
[BurstCompile]
[RequireMatchingQueriesForUpdate]
public partial struct ExampleCheapInputSystem : ISystem
{
    [BurstCompile]
    void OnCreate(ref SystemState state)
    {
        state.RequireForUpdate<AiFrameStateData>();
        state.RequireForUpdate<AiFirstFrameTag>(); // only run this on frame 0
    }

    [BurstCompile]
    void OnUpdate(ref SystemState state)
    {
        ExampleCheapInputJob job = new ExampleCheapInputJob();
        state.Dependency = job.ScheduleParallel(state.Dependency);
    }
}