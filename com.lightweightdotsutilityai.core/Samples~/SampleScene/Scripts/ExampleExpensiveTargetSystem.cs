// This periodically searches for new targets for all agents. It always chooses the nearest target.
//
// Note that since this is just an example where no spatial partitioning is done so it is N^2 complexity.
// Not using spatial partitioning would be a bad idea in a production game.
// This makes it run extremely slow compared to most other systems!
// Thus it is an excellent example of an expensive calculation that can be triggered by the AI.
//
// The system runs as fast as possible, but it will only run for agents that need its calculation,
// and only in the specified frame during a scoring interval.

using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Lwduai;

// This job loops through all agents in parallel.
[BurstCompile]
public partial struct ExampleExpensiveTargetJob : IJobEntity
{
    //[ReadOnly] public NativeArray<Entity>.ReadOnly AgentEntities;
    [ReadOnly][DeallocateOnJobCompletion] public NativeArray<Entity> AgentEntities;
    [ReadOnly] public ComponentLookup<LocalToWorld> LtwLookups;
    [ReadOnly] public ComponentLookup<FactionData> FactionLookups;

    // For all agents...
    // Note that this is just a normal system unrelated to AI but with one minor exception:
    // It takes the inputs as a parameter, and early-outs anytime the target-distance input is not marked as needing calculation.
    // If not early-outing, then it will copy the winning target distance (if any) into the input buffer.
    //
    // IMPORTANT: You will need to get agent AI data in the query.
    // This code is for use with auto-scoring enabled.
    // NOTE: With auto-scoring disabled, you would replace "ref DynamicBuffer<AiInputData> inputs" with "AiAgentAspect ai".
    [BurstCompile]
    public void Execute(ref AttackTargetData target, ref DynamicBuffer<AiInputData> inputs, in LocalTransform lt, in FactionData faction)
    {
        // IMPORTANT: Early out if the value is not marked as needing calculation.
        // This code is for use with auto-scoring enabled.
        // NOTE: With auto-scoring disabled, you would use: "if (!ai.NeedsCalculation(AiInputType.MyDistanceToTarget)) { return; }"
        if (!inputs[(int)AiInputType.MyDistanceToTarget].NeedsCalculation()) { return; }

        target.TargetEntity = Entity.Null;
        target.Distance = float.MaxValue;

        // for all agents that this agent could attack...
        for (int i = 0; i < AgentEntities.Length; i++)
        {
            Entity otherAgentEntity = AgentEntities[i];
            FactionType otherAgentFaction = FactionLookups[otherAgentEntity].Faction;
            // ignore any agents on the same team
            if (faction.Faction != otherAgentFaction)
            {
                float3 otherAgentPos = LtwLookups[otherAgentEntity].Position;
                float distSq = math.distancesq(lt.Position, otherAgentPos);
                // replace current smallest entity/dist-sq with new one if this one is smaller
                if (distSq < target.Distance)
                {
                    target.Distance = distSq;
                    target.TargetEntity = AgentEntities[i];
                }
            }
        }

        // If no player entities were found, then target distance will be float.MaxValue and target entity will be null
        // otherwise, a target and its distance will be set
        if (target.TargetEntity != Entity.Null)
        {
            // need to do sqrt to get actual distance
            target.Distance = math.sqrt(target.Distance);
        }


        // IMPORTANT: You MUST set the input when the calculation is complete.
        // This is how you do it when auto-scoring is enabled.
        // NOTE: with auto-scoring disabled, you would use ai.ScoreInput(AiInputType.MyDistanceToTarget, target.Distance);
        inputs[(int)AiInputType.MyDistanceToTarget] = new AiInputData(target.Distance);
    }
}


[RequireMatchingQueriesForUpdate]
[BurstCompile]
public partial struct ExampleExpensiveTargetSystem : ISystem
{
    EntityQuery m_agentQuery;

    void OnCreate(ref SystemState state)
    {
        // queries for all agents
        m_agentQuery = state.GetEntityQuery(ComponentType.ReadOnly<LocalToWorld>(), ComponentType.ReadOnly<AgentTag>());

        // IMPORTANT: This ensures that this system's job only executes during its designated frame
        // This line is the same no matter what auto-scoring's value is.
        state.RequireForUpdate<AiFirstFrameTag>();
    }

    [BurstCompile]
    void OnUpdate(ref SystemState state)
    {
        ExampleExpensiveTargetJob job = new ExampleExpensiveTargetJob();
        // sync way
        job.AgentEntities = m_agentQuery.ToEntityArray(Allocator.TempJob);

        // async way
        //job.AgentEntities = m_agentQuery.ToEntityListAsync(Allocator.TempJob, out JobHandle h1).AsReadOnly();
        //state.Dependency = JobHandle.CombineDependencies(state.Dependency, h1);

        job.LtwLookups = SystemAPI.GetComponentLookup<LocalToWorld>(true);
        job.FactionLookups = SystemAPI.GetComponentLookup<FactionData>(true);
        state.Dependency = job.ScheduleParallel(state.Dependency);
    }
}