// This is an authoring tool that is used to make a singleton entity with a dynamic buffer containing
// a bunch of prefab entities that can later be used for instantiation.

using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public struct PrefabData : IBufferElementData
{
    public Entity PrefabEntity;
}

public class PrefabCollectionAuthoring : MonoBehaviour
{
    public List<GameObject> Prefabs;
}

public class PrefabCollectionBaker : Baker<PrefabCollectionAuthoring>
{
    public override void Bake(PrefabCollectionAuthoring authoring)
    {
        Entity prefabCollectionEntity = GetEntity(TransformUsageFlags.None);

        DynamicBuffer<PrefabData> prefabBuf = AddBuffer<PrefabData>(prefabCollectionEntity);

        for (int i = 0; i < authoring.Prefabs.Count; i++) {
            prefabBuf.Add(new PrefabData { PrefabEntity = GetEntity(authoring.Prefabs[i], TransformUsageFlags.Dynamic) });
        }
    }
}