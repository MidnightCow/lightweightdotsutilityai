// This is an example spawner controller class that should be placed in the scene
// and connected to some buttons for performing spawning functions.


using TMPro;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public GameObject GroundPlane = null;
    public TMP_InputField SpawnEnemyCountInput;
    public TMP_InputField SpawnPlayerCountInput;

    Bounds m_bounds = default;

    public void Start()
    {
        UnityEngine.Random.InitState(42);

        if (GroundPlane == null) {
            UnityEngine.Debug.Log($"Ground plane is null on the ExampleSpawner object!!");
            return;
        }
        m_bounds = new Bounds(GroundPlane.transform.position, new UnityEngine.Vector3(GroundPlane.transform.localScale.x, 1f, GroundPlane.transform.localScale.z));
    }

    public void SpawnEnemies()
    {
        Spawn(true);
    }

    public void SpawnPlayers()
    {
        Spawn(false);
    }

    public void Spawn(bool isEnemySpawn)
    {
        EntityManager em = World.DefaultGameObjectInjectionWorld.EntityManager;
        EntityQuery query = em.CreateEntityQuery(ComponentType.ReadOnly<PrefabData>());
        DynamicBuffer<PrefabData> prefabs = query.GetSingletonBuffer<PrefabData>();

        int numberToSpawn = isEnemySpawn ? int.Parse(SpawnEnemyCountInput.text) : int.Parse(SpawnPlayerCountInput.text);

        int prefabIndex = 0;
        int spawnCount = 0;
        while (spawnCount < numberToSpawn) {
            Entity prefabEntity = prefabs[prefabIndex].PrefabEntity;
            if ((isEnemySpawn && em.HasComponent<EnemyTag>(prefabEntity)) || (!isEnemySpawn && em.HasComponent<PlayerTag>(prefabEntity))) {
                Entity newEntity = em.Instantiate(prefabEntity);
                LocalTransform lt = em.GetComponentData<LocalTransform>(newEntity);
                lt.Position.y = 0.5f;
                lt.Position.x = UnityEngine.Random.Range(m_bounds.min.x, m_bounds.max.x);
                lt.Position.z = UnityEngine.Random.Range(m_bounds.min.z, m_bounds.max.z);
                em.SetComponentData(newEntity, lt);
                spawnCount++;
            }
            prefabIndex++;
            if (prefabIndex >= prefabs.Length) { prefabIndex = 0; } // wrap
        }
        query.Dispose();
    }

    public void ClearMap()
    {
        EntityManager em = World.DefaultGameObjectInjectionWorld.EntityManager;
        EntityQuery query = em.CreateEntityQuery(ComponentType.ReadOnly<HealthData>());
        em.DestroyEntity(query);
    }
}